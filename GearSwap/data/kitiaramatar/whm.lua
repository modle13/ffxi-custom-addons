-- https://github.com/Kinematics/GearSwap-Jobs/blob/master/WHM.lua

-------------------------------------------------------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified.
-------------------------------------------------------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
    mote_include_version = 2

    -- Load and initialize the include file.
    include('Mote-Include.lua')
end

local chat = windower.add_to_chat
resources = require('resources')

-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
    state.Buff['Afflatus Solace'] = buffactive['Afflatus Solace'] or false
    state.Buff['Afflatus Misery'] = buffactive['Afflatus Misery'] or false
end

function determine_if_have_item(match_item, default_item)
    local item_array = {}
    local bags = {0,8,10,11,12} --inventory,wardrobe1-4
    local get_items = windower.ffxi.get_items
    local target_item = match_item
    local result_item = default_item

    for i=1,#bags do
        for _,item in ipairs(get_items(bags[i])) do
            if item.id ~= 0 then
                local item_name = resources.items[tonumber(item.id)].name
                if item_name == match_item then
                    result_item = target_item
                end
            end
        end
    end
    return result_item
end

local grip_default = "Oneiros Grip"
local grip_pdt = "Oneiros Grip"
local main_heal = "Tamaxchi"
local main_macc = "Bolelabunga"
local main_idle = "Bolelabunga"
local shield_default = "Sors Shield"
local alaunus_fc = "Alaunus's Cape"

-------------------------------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file.
-------------------------------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()
    state.OffenseMode:options('None', 'Normal')
    state.CastingMode:options('Normal', 'Resistant')
    state.IdleMode:options('Normal', 'PDT')

    select_default_macro_book()

    main_heal = determine_if_have_item('Daybreak', 'Tamaxchi')
    main_macc = determine_if_have_item('Daybreak', 'Bolelabunga')
    main_idle = determine_if_have_item('Daybreak', 'Bolelabunga')
    chat(82, 'heal weap is ' .. main_heal)
    chat(82, 'macc weap is ' .. main_macc)
    chat(82, 'idle weap is ' .. main_idle)
end

-- Define sets and vars used by this job file.
function init_gear_sets()
    --------------------------------------
    -- Start defining the sets
    --------------------------------------

    -- Precast Sets

    -- Fast cast sets for spells
    sets.precast.FC = {
        -- main="Venabulum",ammo="Incantor Stone",
        head="Welkin Crown",
        neck="Voltsurge Torque",
        -- neck="Orison Locket",
        ear1="Malignance Earring",ear2="Loquacious Earring",
        body="Inyanga Jubbah +1",
        back=alaunus_fc,waist="Witful Belt",legs="Orvail Pants +1"
    }

    sets.precast.FC['Enhancing Magic'] = set_combine(sets.precast.FC, {waist="Siegel Sash", feet="Theo. Duckbills +1"})

    sets.precast.FC.Stoneskin = set_combine(sets.precast.FC['Enhancing Magic'], {})

    sets.precast.FC['Healing Magic'] = set_combine(sets.precast.FC, {main="Vadose Rod",sub=shield_default,legs="Ebers Pantaloons +1"})

    sets.precast.FC.StatusRemoval = sets.precast.FC['Healing Magic']

    sets.precast.FC.Cure = set_combine(sets.precast.FC['Healing Magic'], {})
    sets.precast.FC.Curaga = sets.precast.FC.Cure
    sets.precast.FC.CuragaWeather = sets.precast.FC.Cure
    sets.precast.FC.CureSolace = sets.precast.FC.Cure
    sets.precast.FC.CureWeather = sets.precast.FC.Cure
    sets.precast.FC.CuragaWeather = sets.precast.FC.Cure
    -- CureMelee spell map should default back to Healing Magic.

    -- Precast sets to enhance JAs
    sets.precast.JA.Benediction = {}

    -- Waltz set (chr and vit)
    sets.precast.Waltz = {}


    -- Weaponskill sets

    -- Default set for any weaponskill that isn't any more specifically defined
    gear.default.weaponskill_neck = "Asperity Necklace"
    gear.default.weaponskill_waist = ""
    sets.precast.WS = {
        neck=gear.ElementalGorget,
        body="Ayanmo Corazza +2",hands="Aya. Manopolas +1",ring1="Rajas Ring",
        waist=gear.ElementalBelt
    }

    sets.precast.WS['Flash Nova'] = {
        neck="Stoicheion Medal",ear1="Friomisi Earring",ear2="Hecate's Earring",
        body="Inyanga Jubbah +1",ring1="Rajas Ring",ring2="Strendu Ring",
        back="Toro Cape",waist="Thunder Belt"
    }

    -- Midcast Sets

    sets.midcast.FastRecast = {
        ear2="Loquacious Earring",
        body="Inyanga Jubbah +1",
        back=alaunus_fc
    }

    -- Cure sets
    gear.default.obi_waist = "Witful Belt"
    gear.default.obi_back = "Pahtli Cape"

    sets.midcast.CureSolace = {
        main="Daybreak",sub=shield_default,ammo="Incantor Stone",
        head="Ebers Cap +1",neck="Orison Locket",ear1="Nourishing Earring",ear2="Orison Earring",
        body="Ebers Bliaut",hands="Inyanga Dastanas +2",ring1="Janniston Ring",ring2="Inyanga Ring",
        back=aluanus_fc,waist=gear.ElementalObi,legs="Ebers Pantaloons +1",feet="Inyanga Crackows +2"
    }

    -- sets.midcast.CureWeather = set_combine(sets.midcast.CureSolace, {main="Chatoyant Staff",sub=grip_default})

    sets.midcast.CureNormal = set_combine(sets.midcast.CureSolace, {})

    sets.midcast.CuragaNormal = {
        main=main_heal,sub=shield_default,ammo="Incantor Stone",
    -- sets.midcast.CuragaNormal = {ammo="Incantor Stone",
        head="Ebers Cap +1",neck="Orison Locket",ear2="Orison Earring",
        body="Inyanga Jubbah +1",ring1="Janniston Ring",
        back="Alaunus's Cape",waist=gear.ElementalObi,legs="Ebers Pantaloons +1"
    }

    sets.midcast.CuragaWeather = set_combine(sets.midcast.Curaga, {main="Chatoyant Staff",sub=grip_default})

    sets.midcast.CureMelee = {
        ammo="Incantor Stone",
        head="Ebers Cap +1",neck="Orison Locket",ear2="Orison Earring",
        body="Inyanga Jubbah +1",
        back="Pahtli Cape",waist=gear.ElementalObi,legs="Ebers Pantaloons +1"
    }

    sets.midcast.Cursna = {
        main=my_main,sub=my_sub,
        head="Ebers Cap +1",neck="Malison Medallion",ear1="Healing Earring",
        body="Ebers Bliaut",hands="Inyanga Dastanas +2",ring2="Ephedra Ring",
        back=alaunus_fc
    }

    sets.midcast.StatusRemoval = {
        head="Ebers Cap +1",neck="Cleric's Torque",legs="Ebers Pantaloons +1"
    }

    -- 110 total Enhancing Magic Skill; caps even without Light Arts
    sets.midcast['Enhancing Magic'] = {
        main=my_main,sub=my_sub,
        neck="Melic Torque",ear1="Andoaa Earring",
        hands="Inyanga Dastanas +2",
        waist="Embla Sash",feet="Ebers Duckbills"
    }

    sets.midcast.Haste = {waist="Embla Sash"}

    sets.midcast.Stoneskin = set_combine(sets.midcast['Enhancing Magic'], {
        neck="Orison Locket",ear2="Loquacious Earring",
        body="Inyanga Jubbah +1",
        back=alaunus_fc,waist="Siegel Sash"
    })

    sets.midcast.Auspice = set_combine(sets.midcast['Enhancing Magic'], {feet="Ebers Duckbills"})

    sets.midcast.BarElement = set_combine(sets.midcast['Enhancing Magic'], {
        main=my_main,sub=my_sub,
        head="Ebers Cap +1",
        body="Ebers Bliaut",hands="Ebers Mitts",
        legs="Ebers Pant. +1",feet="Ebers Duckbills"
    })

    sets.midcast.Regen = {
        main="Bolelabunga",sub=my_sub,
        head="Inyanga Tiara +2",
        body="Piety Briault +1",hands="Ebers Mitts",
        waist="Embla Sash"
    }

    sets.midcast.Protectra = set_combine(sets.midcast['Enhancing Magic'], {ear1="Brachyura Earring"})
    sets.midcast.Shellra = set_combine(sets.midcast['Enhancing Magic'], {ear1="Brachyura Earring"})

    sets.midcast.SIRD = {range="Staunch Tathlum",hands="Chironic Gloves",}
    sets.midcast['Arise'] = set_combine(sets.midcast.SIRD, {})
    sets.midcast['Raise III'] = set_combine(sets.midcast.SIRD, {})
    sets.midcast['Aquaveil'] = set_combine(sets.midcast.SIRD, {})

    sets.midcast['Divine Magic'] = {main=my_main,sub=my_sub,ring2="Perception Ring"}

    sets.midcast['Dark Magic'] = {
        main="Venabulum", sub=my_sub,
        neck="Deceiver's Torque",
        ring1="Strendu Ring",ring2="Perception Ring",
        waist="Austerity Belt +1"
    }

    -- Custom spell classes
    sets.midcast.MndEnfeebles = {
        main="Venabulum",sub="Tokko Grip",
        head="Inyanga Tiara +2",neck="Weike Torque",
        body="Vanya Robe",hands="Inyanga Dastanas +2",ring1="Strendu Ring",ring2="Perception Ring",
        waist="Tarutaru Sash",
        legs={ name="Chironic Hose", augments={'Accuracy+23','Weapon Skill Acc.+8','STR+2 DEX+2','Accuracy+19 Attack+19','Mag. Acc.+20 "Mag.Atk.Bns."+20',}},
        feet="Theo. Duckbills +1"
    }

    sets.midcast.IntEnfeebles = set_combine(sets.midcast.MndEnfeebles, {})

    sets.midcast.ReposeDivine = set_combine(sets.midcast.MndEnfeebles, {
        neck="Sanctity Necklace",
        back="Pahtli Cape",waist=gear.ElementalObi,legs="Inyanga Shalwar +2",
        -- feet="Theo. Duckbills +1"
        feet="Inyanga Crackows +2"
    })

    -- Sets to return to when not performing an action.

    -- Resting sets
    sets.resting = {
        main="Chatoyant Staff",sub=grip_default,
        head="Wivre Hairpin",
        body="Ebers Bliaut",hands="Inyanga Dastanas +2",ring2="Inyanga Ring",
        waist="Austerity Belt +1",feet="Inyanga Crackows +2"
    }

    -- Idle sets (default idle set not needed since the other three are defined, but leaving for testing purposes)
    -- sets.idle = {
        -- main="Tamaxchi",sub="Sors Shield",ammo="Incantor Stone",
    sets.idle = {
        main="Daybreak",sub="Sors Shield",ammo="Homiliary",
        head="Inyanga Tiara +2",neck="Twilight Torque",ear1="Hearty Earring",ear2="Loquacious Earring",
        body="Ischemia Chasuble",hands="Inyanga Dastanas +2",ring1="Defending Ring",ring2="Inyanga Ring",
        back="Iximulew Cape",waist="Witful Belt",
        legs={ name="Chironic Hose", augments={'Pet: VIT+8','Pet: AGI+9','"Refresh"+2','Mag. Acc.+15 "Mag.Atk.Bns."+15',}},
        feet="Inyanga Crackows +2"
    }

    sets.idle.PDT = {
        main="Chatoyant Staff", sub=grip_pdt,ammo="Incantor Stone",
        head="Ayanmo Zucchetto +1",neck="Wiglen Gorget",ear1="Hearty Earring",ear2="Loquacious Earring",
        body="Ayanmo Corazza +2",hands="Aya. Manopolas +1",ring2="Dark Ring",
        back="Iximulew Cape",waist="Witful Belt",feet="Aya. Gambieras +2"
    }

    -- sets.idle.Town = {main="Chatoyant Staff",sub=grip_pdt,ammo="Homiliary",
    --     head="Ebers Cap +1",neck="Wiglen Gorget",ear1="Hearty Earring",ear2="Loquacious Earring",
    --     body="Ischemia Chasuble",hands="Chironic Gloves",ring1="Janniston Ring",ring2="Inyanga Ring",
    --     back="Iximulew Cape",waist="Witful Belt",legs="Chironic Hose",feet="Inyanga Crackows +2"}

    sets.idle.Weak = {
        main="Chatoyant Staff",sub=my_sub,ammo="Staunch Tathlum",
        head="Ayanmo Zucchetto +1",neck="Wiglen Gorget",ear2="Loquacious Earring",
        ring2="Dark Ring",
        back="Iximulew Cape",waist="Witful Belt"
    }

    -- Defense sets

    sets.defense.PDT = {
        main="Chatoyant Staff",sub="Achaq Grip",ammo="Staunch Tathlum",
        head="Ayanmo Zucchetto +1",neck="Wiglen Gorget",
        body="Ayanmo Corazza +2",ring2="Dark Ring"
    }

    sets.defense.MDT = {
        main="Chatoyant Staff",sub="Achaq Grip",ammo="Staunch Tathlum",
        neck="Twilight Torque",
        ring1="Inyanga Ring",
        back="Tantalic Cape"
    }

    sets.Kiting = {ring1="Shneddick Ring"}

    sets.latent_refresh = {ammo="Homiliary",ring2="Inyanga Ring",waist="Fucho-no-Obi"}

    -- Engaged sets

    -- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
    -- sets if more refined versions aren't defined.
    -- If you create a set with both offense and defense modes, the offense mode should be first.
    -- EG: sets.engaged.Dagger.Accuracy.Evasion

    -- Basic set for if no TP weapon is defined.
    sets.engaged = {
        head="Ayanmo Zucchetto +1",neck="Lissome Necklace",ear1="Brutal Earring",
        body="Ayanmo Corazza +2",hands="Ayanmo Manopolas +1",ring1="Rajas Ring",ring2="Enlivened Ring",
        waist="Windbuffet Belt",legs="Ayanmo Cosciales +1",feet="Ayanmo Gambieras +2"
    }

    -- Buff sets: Gear that needs to be worn to actively enhance a current player buff.
    sets.buff['Divine Caress'] = {hands="Ebers Mitts"}
end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for standard casting events.
-------------------------------------------------------------------------------------------------------------------

local bufflist = {"Protect", "Shell", "Haste", "Auspice", "Sublimation: Activated"}

function check_buffs()
    -- 81 = green
    -- 82 = purple
    -- 83 = blue

    local color = 82
    -- if areas.Cities:contains(world.area) then
    --     return
    -- end

    local missing = {}

    for k,v in pairs(bufflist) do
        if not buffactive[v] then
            table.insert(missing, v)
        end
    end

    if player.sub_job:lower() == 'sch' and world.weather_element ~= "Light" then
        table.insert(missing, "Aurorastorm")
    end

    if table.getn(missing) > 0 then
        chat(color, " ")
        chat(color, "MISSING               " .. table.concat(missing, "   "))
        chat(color, " ")
    end

end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
-- Set eventArgs.useMidcastGear to true if we want midcast gear equipped on precast.
function job_precast(spell, action, spellMap, eventArgs)
    if spell.english == "Paralyna" and buffactive.Paralyzed then
        -- no gear swaps if we're paralyzed, to avoid blinking while trying to remove it.
        eventArgs.handled = true
    end

    check_buffs()

    if spell.skill == 'Healing Magic' then
        gear.default.obi_back = "Mending Cape"
    else
        gear.default.obi_back = "Toro Cape"
    end
end

-- function job_midcast(spell, action, spellMap, eventArgs)
    -- midcast stuff here
-- end

function job_post_midcast(spell, action, spellMap, eventArgs)

    -- Apply Divine Caress boosting items as highest priority over other gear, if applicable.
    if spellMap == 'StatusRemoval' and buffactive['Divine Caress'] then
        equip(sets.buff['Divine Caress'])
    end
end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for non-casting events.
-------------------------------------------------------------------------------------------------------------------

-- Handle notifications of general user state change.
function job_state_change(stateField, newValue, oldValue)
    if stateField == 'Offense Mode' then
        if newValue == 'Normal' then
            disable('main','sub','range')
        else
            enable('main','sub','range')
        end
    end
end


-------------------------------------------------------------------------------------------------------------------
-- User code that supplements standard library decisions.
-------------------------------------------------------------------------------------------------------------------

-- Custom spell mapping.
function job_get_spell_map(spell, default_spell_map)
    if spell.action_type == 'Magic' then
        if (default_spell_map == 'Cure' or default_spell_map == 'Curaga') and player.status == 'Engaged' then
            return "CureMelee"
        elseif default_spell_map == 'Curaga' then
            if world.weather_element == "Light" then
                return "CuragaWeather"
            end
            return "CuragaNormal"
        elseif default_spell_map == 'Cure' then
            if state.Buff['Afflatus Solace'] then
                if world.weather_element == "Light" then
                    -- chat(color, "light cure")
                    return "CureWeather"
                end
                return "CureSolace"
            end
            return "CureNormal"
        elseif spell.skill == "Enfeebling Magic" then
            if spell.type == "WhiteMagic" then
                return "MndEnfeebles"
            else
                return "IntEnfeebles"
            end
        elseif spell.skill == "Divine Magic" then
            if spell.name == "Repose" then
                return "ReposeDivine"
            end
        end
    end
end

function customize_idle_set(idleSet)
    if player.mpp < 51 then
        idleSet = set_combine(idleSet, sets.latent_refresh)
    end
    return idleSet
end

-- Called by the 'update' self-command.
function job_update(cmdParams, eventArgs)
    if cmdParams[1] == 'user' and not areas.Cities:contains(world.area) then
        local needsArts =
            player.sub_job:lower() == 'sch' and
            not buffactive['Light Arts'] and
            not buffactive['Addendum: White'] and
            not buffactive['Dark Arts'] and
            not buffactive['Addendum: Black']

        if not buffactive['Afflatus Solace'] and not buffactive['Afflatus Misery'] then
            if needsArts then
                send_command('@input /ja "Afflatus Solace" <me>;wait 1.2;input /ja "Light Arts" <me>')
            else
                send_command('@input /ja "Afflatus Solace" <me>;wait 1.2')
            end
        end
    end
end


-- Function to display the current relevant user state when doing an update.
function display_current_job_state(eventArgs)
    display_current_caster_state()
    eventArgs.handled = true
end

-------------------------------------------------------------------------------------------------------------------
-- Utility functions specific to this job.
-------------------------------------------------------------------------------------------------------------------

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
    -- Default macro set/book
    set_macro_page(1, 3)
end
