local chat = windower.add_to_chat

-------------------------------------------------------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified.
-------------------------------------------------------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
    mote_include_version = 2

    -- Load and initialize the include file.
    include('Mote-Include.lua')
end


-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
    state.Buff.Saboteur = buffactive.saboteur or false
    include('Mote-TreasureHunter')
    send_command('bind ^= gs c cycle treasuremode')
end

-------------------------------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file.
-------------------------------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()
    state.OffenseMode:options('None', 'Normal')
    state.HybridMode:options('Normal', 'PhysicalDef', 'MagicalDef')
    state.CastingMode:options('Normal', 'Resistant')
    state.IdleMode:options('Normal', 'PDT', 'MDT')

    gear.default.obi_waist = "Sekhmet Corset"

    select_default_macro_book()
end


-- Define sets and vars used by this job file.
function init_gear_sets()
    --------------------------------------
    -- Start defining the sets
    --------------------------------------
    sets.TreasureHunter = {
        head="White Rarab Cap +1",
        hands={ name="Chironic Gloves", augments={'Attack+13','Accuracy+10','"Treasure Hunter"+2',}},
        waist="Chaac Belt",
    }

    -- Precast Sets

    -- Precast sets to enhance JAs
    sets.precast.JA['Chainspell'] = {body="Vitiation Tabard"}


    -- Waltz set (chr and vit)
    sets.precast.Waltz = {
        head="Atrophy Chapeau +1",
        body="Atrophy Tabard",hands="Yaoyotl Gloves",
        back="Refraction Cape",legs="Hagondes Pants",feet="Hagondes Sabots"
    }

    -- Don't need any special gear for Healing Waltz.
    sets.precast.Waltz['Healing Waltz'] = {}

    -- Fast cast sets for spells

    sets.precast.FC = {
        ammo="Sapience Orb",
        head={ name="Merlinic Hood", augments={'Mag. Acc.+4 "Mag.Atk.Bns."+4','"Fast Cast"+7','CHR+8','"Mag.Atk.Bns."+12',}}, -- 15
        neck="Voltsurge Torque",
        ear1="Malignance Earring", -- 4
        ear2="Loquacious Earring",
        body={ name="Merlinic Jubbah", augments={'"Fast Cast"+7','MND+10','Mag. Acc.+6',}}, -- 13
        hands={ name="Merlinic Dastanas", augments={'"Fast Cast"+7','INT+10','Mag. Acc.+14',}}, -- 7
        back="Swith Cape", -- 3
        waist="Embla Sash", -- 5
        legs="Orvail Pants +1", -- 5
        feet={ name="Carmine Greaves", augments={'HP+60','MP+60','Phys. dmg. taken -3',}}, -- 7
    }

    sets.precast.FC.disengaged = set_combine(sets.precast.FC, {
        main={ name="Emissary", augments={'DMG:+15','Accuracy+15','Mag. Acc.+15',}},
    })

    sets.precast.FC.disengaged.DW = set_combine(sets.precast.FC.disengaged, {
        main={ name="Emissary", augments={'DMG:+15','Accuracy+15','Mag. Acc.+15',}},
        sub={ name="Emissary", augments={'Mag. Acc.+15','"Mag.Atk.Bns."+20','"Refresh"+1',}},
    })

    sets.precast.FC.Impact = set_combine(sets.precast.FC, {head=empty,body="Twilight Cloak"})

    -- Weaponskill sets
    -- Default set for any weaponskill that isn't any more specifically defined
    sets.precast.WS = {
        head="Jhakri Coronal +1",neck="Lissome Necklace",ear1="Ishvara Earring",ear2="Moonshade Earring",
        body="Jhakri Robe +2",hands="Jhakri Cuffs +2",ring1="Rajas Ring",ring2="Excelsis Ring",
        back="Atheling Mantle",waist="Caudata Belt",legs="Jhakri Slops +1",feet="Jhakri Slops +1"
    }

    sets.precast.WS['Savage Blade'] = set_combine(sets.precast.WS, {})

    -- Specific weaponskill sets.  Uses the base set if an appropriate WSMod version isn't found.
    sets.precast.WS['Requiescat'] = set_combine(sets.precast.WS, {
        neck="Soil Gorget",ear1="Brutal Earring",ear2="Moonshade Earring",
        ring1="Aquasoul Ring",ring2="Aquasoul Ring",waist="Soil Belt"
    })

    sets.precast.WS['Sanguine Blade'] = {
        ammo="Witchstone",
        head="Hagondes Hat",neck="Eddy Necklace",ear1="Friomisi Earring",ear2="Hecate's Earring",
        body="Hagondes Coat",hands="Yaoyotl Gloves",ring1="Strendu Ring",ring2="Acumen Ring",
        back="Toro Cape",legs="Hagondes Pants",feet="Hagondes Sabots"
    }

    sets.precast.WS['Aeolian Edge'] = {
        head="Welkin Crown",neck="Sanctity Necklace",ear1="Malignance Earring",ear2="Friomisi Earring",
        body="Jhakri Robe +2",hands="Jhakri Cuffs",ring1="Fortified Ring",ring2="Jhakri Ring",
        back="Toro Cape",waist="Chiner's Belt +1",legs="Jhakri Slops +1",feet="Jhakri Pigaches +1",
    }

    -- Midcast Sets

    sets.midcast.FastRecast = {
        head="Atrophy Chapeau +1",ear2="Loquacious Earring",
        body="Vitiation Tabard",hands="Gendewitha Gages",ring1="Prolix Ring",
        back="Swith Cape +1",waist="Witful Belt",legs="Hagondes Pants",feet="Hagondes Sabots"
    }

    sets.midcast.Cure = {
        head="Vanya Hood",neck="Phalaina Locket",ear1="Mendicant's Earring",ear2="Loquacious Earring",
        body="Chironic Doublet",
        hands="Gendewitha Gages",
        ring1="Ephedra Ring",ring2="Lebeche Ring",
        back="Solemnity Cape",waist="Austerity Belt +1",legs="Gyve Trousers",feet="Medium's Sabots"
    }

    sets.midcast.Curaga = sets.midcast.Cure
    sets.midcast.CureSelf = {ring1="Kunaji Ring",ring2="Asklepian Ring"}

    sets.midcast.Cursna = {
        main=my_main,sub=my_sub,
        neck="Malison Medallion",ear1="Healing Earring",
        body="Vitiation Tabard",ring1="Ephedra Ring"
    }

    sets.midcast['Enhancing Magic'] = {
        head="Atrophy Chapeau +1",neck="Colossus's Torque",
        body="Vitiation Tabard",hands="Atrophy Gloves +2",ring1="Prolix Ring",
        back="Estoqueur's Cape",waist="Embla Sash",legs="Atrophy Tights +2",feet="Lethargy Houseaux +1"
    }

    sets.midcast.Refresh = {legs="Lethargy Fuseau +1",waist="Gishdubar Sash"}

    sets.midcast.Stoneskin = {waist="Siegel Sash"}

    sets.midcast['Enfeebling Magic'] = {
        main="Naegling",sub="Tauret",
        ammo="Hydrocera",
        head="Malignance Chapeau",neck="Sanctity Necklace",ear1="Malignance Earring",ear2="Gwati Earring",
        body="Jhakri Robe +2",hands="Jhakri Cuffs +1",ring1="Ayanmo Ring",ring2="Perception Ring",
        back="Ogapepo Cape",waist="Ovate Rope",legs="Malignance Tights",feet="Malignance Boots"
    }

    sets.midcast['Dia III'] = set_combine(sets.midcast['Enfeebling Magic'], {head="Vitiation Chapeau"})

    sets.midcast['Slow II'] = set_combine(sets.midcast['Enfeebling Magic'], {head="Vitiation Chapeau"})

    sets.midcast['Elemental Magic'] = {
        -- main="Lehbrailg +2",sub="Zuuxowu Grip",ammo="Dosis Tathlum",
        head="Jhakri Coronal +1",neck="Sanctity Necklace",ear1="Friomisi Earring",ear2="Malignance Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +1",ring1="Mephitas's Ring +1",ring2="Jhakri Ring",
        back="Izdubar Mantle",waist=gear.ElementalObi,legs="Jhakri Slops +1",feet="Jhakri Pigaches +1"
    }

    sets.midcast.Impact = set_combine(sets.midcast['Elemental Magic'], {head=empty,body="Twilight Cloak"})

    sets.midcast['Dark Magic'] = {
        -- main="Lehbrailg +2",sub="Mephitis Grip",ammo="Kalboron Stone",
        head="Atrophy Chapeau +1",neck="Henic Torque",ear1="Lifestorm Earring",ear2="Psystorm Earring",
        body="Vanir Cotehardie",hands="Gendewitha Gages",ring1="Prolix Ring",ring2="Sangoma Ring",
        back="Refraction Cape",waist="Goading Belt",legs="Bokwus Slops",feet="Bokwus Boots"
    }

    --sets.midcast.Stun = set_combine(sets.midcast['Dark Magic'], {})

    sets.midcast.Drain = set_combine(sets.midcast['Dark Magic'], {
        ring1="Excelsis Ring", waist="Fucho-no-Obi"
    })

    sets.midcast.Aspir = sets.midcast.Drain


    -- Sets for special buff conditions on spells.

    sets.midcast.EnhancingDuration = set_combine(sets.midcast['Enhancing Magic'], {
        hands="Atrophy Gloves +1",back="Sucellos's Cape",waist="Embla Sash",feet="Lethargy Houseaux +1"
    })

    sets.buff.ComposureOther = set_combine(sets.midcast.EnhancingDuration, {
        head="Lethargy Chappel +1",
        body="Lethargy Sayon +1",hands="Lethargy Gantherots +1",
        legs="Lethargy Fuseau +1",feet="Lethargy Houseaux +1"
    })

    sets.buff.Saboteur = {hands="Lethargy Gantherots +1"}

    -- Sets to return to when not performing an action.

    -- Resting sets
    sets.resting = {
        -- main="Chatoyant Staff",
        head="Vitiation Chapeau",neck="Wiglen Gorget",
        body="Atrophy Tabard",hands="Serpentes Cuffs",ring1="Sheltered Ring",ring2="Paguroidea Ring",
        waist="Austerity Belt",
        -- legs="Nares Trews",
        feet="Chelona Boots +1"
    }

    -- Idle sets
    sets.idle = {
        main="Emissary",sub="Demersal Degen",
        ammo="Homiliary",
        head="Jhakri Coronal +1",neck="Sanctity Necklace",ear1="Eabani Earring",ear2="Steelflash Earring",
        body="Jhakri Robe +1",
        hands="Ayanmo Manopolas +2",ring1="K'ayres Ring",ring2="Stikini Ring +1",
        back="Solemnity Cape",waist="Flume Belt",legs="Carmine Cuisses +1",feet="Ayanmo Gambieras +2"
    }

    sets.idle.Town = set_combine(sets.idle, {
        ammo="Impatiens",
        head="Malignance Chapeau",
        legs="Carmine Cuisses +1",feet="Malignance Boots"
    })

    -- sets.idle.Weak = {
    --     -- main="Bolelabunga",sub="Genmei Shield",
    --     ammo="Impatiens",
    --     head="Vitiation Chapeau",neck="Wiglen Gorget",ear1="Bloodgem Earring",ear2="Loquacious Earring",
    --     body="Atrophy Tabard",hands="Serpentes Cuffs",ring1="Sheltered Ring",ring2="Paguroidea Ring",
    --     back="Solemnity Cape",waist="Flume Belt",legs="Carmine Cuisses +1",feet="Malignance Boots"}

    sets.idle.PDT = {
        -- main="Bolelabunga",sub="Genmei Shield",
        main="Earth Staff",sub="Mensch Strap",
        ammo="Staunch Tathlum",
        head="Malignance Chapeau",neck="Wiglen Gorget",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Ayanmo Corazza +1",hands="Ayanmo Manopolas +1",ring1="Fortified Ring",ring2="Vertigo Ring",
        back="Solemnity Cape",waist="Flume Belt",feet="Malignance Boots"
    }

    sets.idle.MDT = {
        -- main="Bolelabunga",sub="Genmei Shield",
        ammo="Staunch Tathlum",
        head="Gendewitha Caubeen +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Gendewitha Caubeen +1",hands="Yaoyotl Gloves",ring1="Defending Ring",ring2="Shadow Ring",
        back="Engulfer Cape",waist="Flume Belt",legs="Osmium Cuisses",feet="Gendewitha Galoshes"
    }


    -- Defense sets
    sets.defense.PDT = {
        ammo="Staunch Tathlum",
        head="Atrophy Chapeau +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Hagondes Coat",hands="Gendewitha Gages",ring1="Defending Ring",ring2=gear.DarkRing.physical,
        back="Solemnity Cape",waist="Flume Belt",legs="Hagondes Pants",feet="Gendewitha Galoshes"
    }

    sets.defense.MDT = {
        ammo="Staunch Tathlum",
        head="Atrophy Chapeau +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Atrophy Tabard",hands="Yaoyotl Gloves",ring1="Defending Ring",ring2="Shadow Ring",
        back="Engulfer Cape",waist="Flume Belt",legs="Bokwus Slops",feet="Gendewitha Galoshes"
    }

    sets.Kiting = {legs="Carmine Cuisses +1"}

    sets.latent_refresh = {waist="Fucho-no-obi"}

    -- Engaged sets

    -- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
    -- sets if more refined versions aren't defined.
    -- If you create a set with both offense and defense modes, the offense mode should be first.
    -- EG: sets.engaged.Dagger.Accuracy.Evasion

    -- Normal melee group
    sets.engaged = {
        -- main="Wind Knife",sub="Wind Knife",
        main="Emissary",sub="Demersal Degen",
        ammo="Dosis Tathlum",
        head="Jhakri Coronal +1",neck="Sanctity Necklace",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Jhakri Robe +1",
        hands="Ayanmo Manopolas +2",
        ring1="Apate Ring",ring2="Jhakri Ring",
        back="Atheling Mantle",
        waist="Sailfi Belt",
        legs="Jhakri Slops +1",feet="Ayanmo Gambieras +2"
    }

    sets.engaged.Defense = {
        ammo="Demonry Stone",
        head="Malignance Chapeau",neck="Asperity Necklace",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Atrophy Tabard",hands="Atrophy Gloves +2",ring1="Rajas Ring",ring2="K'ayres Ring",
        back="Kayapa Cape",waist="Goading Belt",legs="Osmium Cuisses",feet="Malignance Boots"
    }

end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for standard casting events.
-------------------------------------------------------------------------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
-- Set eventArgs.useMidcastGear to true if we want midcast gear equipped on precast.
function job_precast(spell, action, spellMap, eventArgs)
    if spell.action_type == 'Magic' then
        if player.status == "Engaged" then
            equip(sets.precast.FC)
        else
            if S{'NIN','DNC'}:contains(player.sub_job) then
                equip(sets.precast.FC.disengaged.DW)
            else
                equip(sets.precast.FC.disengaged)
            end
        end
    end
end

-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
    if spell.skill == 'Enfeebling Magic' and state.Buff.Saboteur then
        equip(sets.buff.Saboteur)
    elseif spell.skill == 'Enhancing Magic' then
        equip(sets.midcast.EnhancingDuration)
        if buffactive.composure and spell.target.type == 'PLAYER' then
            equip(sets.buff.ComposureOther)
        end
    elseif spellMap == 'Cure' and spell.target.type == 'SELF' then
        equip(sets.midcast.CureSelf)
    end
end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for non-casting events.
-------------------------------------------------------------------------------------------------------------------

-- Handle notifications of general user state change.
function job_state_change(stateField, newValue, oldValue)
    if stateField == 'Offense Mode' then
        if newValue == 'None' then
            enable('main','sub','range')
        else
            disable('main','sub','range')
        end
    end
end

-------------------------------------------------------------------------------------------------------------------
-- User code that supplements standard library decisions.
-------------------------------------------------------------------------------------------------------------------

-- Modify the default idle set after it was constructed.
function customize_idle_set(idleSet)
    if player.mpp < 51 then
        idleSet = set_combine(idleSet, sets.latent_refresh)
    end

    return idleSet
end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)
    display_current_caster_state()
    eventArgs.handled = true
end

-------------------------------------------------------------------------------------------------------------------
-- Utility functions specific to this job.
-------------------------------------------------------------------------------------------------------------------

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
    -- Default macro set/book
    if player.sub_job == 'DNC' then
        set_macro_page(2, 10)
    elseif player.sub_job == 'NIN' then
        set_macro_page(4, 10)
    elseif player.sub_job == 'THF' then
        set_macro_page(4, 10)
    else
        set_macro_page(1, 10)
    end
end
