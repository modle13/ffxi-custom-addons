-- no war, using sam as base https://github.com/Kinematics/GearSwap-Jobs/blob/master/SAM.lua

-------------------------------------------------------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified.
-------------------------------------------------------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
    mote_include_version = 2

    -- Load and initialize the include file.
    include('Mote-Include.lua')
end


-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
    state.Buff.Hasso = buffactive.Hasso or false
    state.Buff.Seigan = buffactive.Seigan or false
    state.Buff.Sekkanoki = buffactive.Sekkanoki or false
    state.Buff.Sengikori = buffactive.Sengikori or false
    state.Buff['Meikyo Shisui'] = buffactive['Meikyo Shisui'] or false
end

-------------------------------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file.
-------------------------------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.
function user_setup()
    state.OffenseMode:options('Normal', 'Acc', 'CP')
    state.HybridMode:options('Normal', 'PDT', 'Reraise')
    state.WeaponskillMode:options('Normal', 'Acc', 'Mod')
    state.PhysicalDefenseMode:options('Normal', 'PDT', 'Reraise')

    update_combat_form()

    -- Additional local binds
    send_command('bind ^` input /ja "Hasso" <me>')
    send_command('bind !` input /ja "Seigan" <me>')
    send_command('bind !f9` input /jump')
    -- send_command('bind f9 gs c ToggleIdle')

    select_default_macro_book()
end


-- Called when this job file is unloaded (eg: job change)
function user_unload()
    send_command('unbind ^`')
    send_command('unbind !-')
end


-- Define sets and vars used by this job file.
function init_gear_sets()
    --------------------------------------
    -- Start defining the sets
    --------------------------------------

    -- Precast Sets
    -- Precast sets to enhance JAs
    sets.TreasureHunter = {head="White rarab cap +1", waist="Chaac Belt"}

    sets.precast.JA.Angon = {ammo="Angon"}
    sets.precast.JA.Jump = {back="Brigantia's Mantle",feet="Vishap Greaves +1"}
    sets.precast.JA['Call Wyvern'] = {boyd="Pteroslaver Mail"}
    sets.precast.JA['High Jump'] = set_combine(sets.precast.JA.Jump, {})
    sets.precast.JA['Soul Jump'] = set_combine(sets.precast.JA.Jump, {legs="Peltast's Cuissots"})
    sets.precast.JA['Spirit Jump'] = set_combine(sets.precast.JA.Jump, {legs="Peltast's Cuissots",feet="Pelt. Schyn. +1"})

    -- Weaponskill sets
    -- Default set for any weaponskill that isn't any more specifically defined
    sets.precast.WS = {ammo="Ginsen",
        head={ name="Valorous Mask", augments={'Mag. Acc.+4 "Mag.Atk.Bns."+4','Pet: Phys. dmg. taken -1%','Weapon skill damage +8%',}},
        neck="Clotharius Torque",ear1="Moonshade Earring",ear2="Sherida Earring",
        -- head="Flamma Zucchetto +2",neck="Ygnas's Resolve +1",ear1="Brutal Earring",ear2="Mache Earring",
        body="Sulevia's Plate. +2",hands="Flamma Manopolas +2",ring1="Flamma Ring",ring2="Mujin Band",
        back={ name="Brigantia's Mantle", augments={'Accuracy+3 Attack+3','STR+2','Weapon skill damage +10%',}},
        waist="Windbuffet Belt",
        legs={ name="Valorous Hose", augments={'VIT+6','STR+5','Weapon skill damage +8%','Accuracy+9 Attack+9','Mag. Acc.+19 "Mag.Atk.Bns."+19',}},
        feet="Sulevia's Leggings +1"}
    sets.precast.WS.Acc = set_combine(sets.precast.WS, {})

    sets.precast.WS["Camlann's Torment"] = set_combine(sets.precast.WS, {waist="Light Belt"})
    sets.precast.WS["Impulse Drive"] = set_combine(sets.precast.WS, {neck="Shadow Gorget"})

    sets.test = {ammo="Ginsen",
        head="Flamma Zucchetto +2",neck="Lissome Necklace",ear1="Brutal Earring",ear2="Mache Earring",
        -- head="Flamma Zucchetto +2",neck="Ygnas's Resolve +1",ear1="Brutal Earring",ear2="Mache Earring",
        body="Sulevia's Plate. +2",hands="Flamma Manopolas +2",ring1="Flamma Ring",ring2="Rufescent Ring",
        back="Brigantia's Mantle",waist="Sailfi Belt +1",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}

    -- Midcast Sets
    sets.midcast.FastRecast = {}

    -- Sets to return to when not performing an action.

    -- Resting sets
    sets.resting = {head="Twilight Helm",neck="Wiglen Gorget"}

    sets.idle = {
        head="Valorous Mask",ear1="Brachyura Earring",ear2="Hearty Earring",
        body="Pteroslaver Mail",hands="Sulevia's Gauntlets +1",ring1="Defending Ring",ring2="Dark Ring",
        back="Solemnity Cape",waist="Flume Belt",legs="Carmine Cuisses +1",feet="Sulevia's Leggings +1"}

    -- Idle sets (default idle set not needed since the other three are defined, but leaving for testing purposes)
    sets.idle.Town = set_combine(sets.idle, {body="Tartarus Platemail"})

    -- Defense sets
    sets.defense.PDT = {
        head="Sulevia's Mask +1",neck="Twilight Torque",ear1="Brachyura Earring",ear2="Hearty Earring",
        body="Tartarus Platemail",hands="Sulevia's Gauntlets +1",ring1="Defending Ring",ring2="Dark Ring",
        back="Solemnity Cape",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}

    sets.idle.Weak = set_combine(sets.defense.PDT, {head="Twilight Helm",body="Twilight Mail"})

    sets.defense.Reraise = set_combine(sets.defense.PDT, {head="Twilight Helm",body="Twilight Mail"})

    sets.defense.MDT = {
        head="Sulevia's Mask +1",neck="Twilight Torque",ear1="Brachyura Earring",ear2="Hearty Earring",
        body="Tartarus Platemail",hands="Sulevia's Gauntlets +1",ring1="Defending Ring",ring2="Dark Ring",
        back="Solemnity Cape",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}

    sets.Kiting = {legs="Carmine Cuisses +1"}

    sets.Reraise = {head="Twilight Helm",body="Twilight Mail"}

    -- Engaged sets

    -- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
    -- sets if more refined versions aren't defined.
    -- If you create a set with both offense and defense modes, the offense mode should be first.
    -- EG: sets.engaged.Dagger.Accuracy.Evasion

    -- Normal melee group
    -- Delay 450 GK, 25 Save TP => 65 Store TP for a 5-hit (25 Store TP in gear)
    sets.engaged = {ammo="Ginsen",
        head="Flamma Zucchetto +2",neck="Clotharius Torque",ear1="Brutal Earring",ear2="Sherida Earring",
        body="Flamma Korazin +2",hands="Flamma Manopolas +2",ring1="Rajas Ring",ring2="Flamma Ring",
        back="Atheling Mantle",waist="Sailfi Belt +1",legs="Sulevi. Cuisses +1",feet="Flamma Gambieras +1"}
    sets.engaged.Acc = {sub="Tzacab Grip",
        head="Flamma Zucchetto +2",neck="Lissome Necklace",ear1="Mache Earring",ear2="Mache Earring",
        body="Sulevia's Platemail +2",hands="Flamma Manopolas +2",ring1="Rajas Ring",
        back="Sokolski Mantle",waist="Sailfi Belt +1",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}
    sets.engaged.PDT = {
        head="Sulevia's Mask +1",neck="Wiglen Gorget",
        body="Tartarus Platemail",hands="Sulevia's Gauntlets +1",ring1="Sulevia's Ring",ring2="Dark Ring",
        back="Solemnity Cape",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}
    sets.engaged.Acc.PDT = {
        neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +2",hands="Flamma Manopolas +2",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Sailfi Belt +1",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}
    sets.engaged.Reraise = {
        head="Twilight Helm",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Twilight Mail",hands="Flamma Manopolas +2",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Sailfi Belt +1",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}
    sets.engaged.Acc.Reraise = {
        neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +2",hands="Flamma Manopolas +2",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Sailfi Belt +1",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings +1"}

    sets.precast.JA['Spirit Link'] = set_combine(sets.engaged, {hands="Peltast's Vambraces"})

end




-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for standard casting events.
-------------------------------------------------------------------------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic target handling to be done.
function job_pretarget(spell, action, spellMap, eventArgs)
    if spell.type == 'WeaponSkill' then
        -- Change any GK weaponskills to polearm weaponskill if we're using a polearm.
        if player.equipment.main=='Quint Spear' or player.equipment.main=='Quint Spear' then
            if spell.english:startswith("Tachi:") then
                send_command('@input /ws "Penta Thrust" '..spell.target.raw)
                eventArgs.cancel = true
            end
        end
    end
end

-- Run after the default precast() is done.
-- eventArgs is the same one used in job_precast, in case information needs to be persisted.
function job_post_precast(spell, action, spellMap, eventArgs)
    if spell.type:lower() == 'weaponskill' then
        if state.Buff.Sekkanoki then
            equip(sets.buff.Sekkanoki)
        end
        if state.Buff.Sengikori then
            equip(sets.buff.Sengikori)
        end
        if state.Buff['Meikyo Shisui'] then
            equip(sets.buff['Meikyo Shisui'])
        end
    end
end


-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
    -- Effectively lock these items in place.
    if state.HybridMode.value == 'Reraise' or
        (state.DefenseMode.value == 'Physical' and state.PhysicalDefenseMode.value == 'Reraise') then
        equip(sets.Reraise)
    end
end

-------------------------------------------------------------------------------------------------------------------
-- User code that supplements standard library decisions.
-------------------------------------------------------------------------------------------------------------------

-- Called by the 'update' self-command, for common needs.
-- Set eventArgs.handled to true if we don't want automatic equipping of gear.
function job_update(cmdParams, eventArgs)
    update_combat_form()
end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)

end

-------------------------------------------------------------------------------------------------------------------
-- Utility functions specific to this job.
-------------------------------------------------------------------------------------------------------------------

function update_combat_form()
    if areas.Adoulin:contains(world.area) and buffactive.ionis then
        state.CombatForm:set('Adoulin')
    else
        state.CombatForm:reset()
    end
end

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
    -- Default macro set/book
    if player.sub_job == 'SAM' then
        set_macro_page(1, 1)
    elseif player.sub_job == 'WHM' then
        set_macro_page(1, 1)
    elseif player.sub_job == 'BLU' then
        set_macro_page(1, 1)
    else
        set_macro_page(1, 1)
    end
end
