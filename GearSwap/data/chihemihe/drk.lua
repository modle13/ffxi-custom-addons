-- no drk, using sam as base https://github.com/Kinematics/GearSwap-Jobs/blob/master/SAM.lua

-------------------------------------------------------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified.
-------------------------------------------------------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
    mote_include_version = 2

    -- Load and initialize the include file.
    include('Mote-Include.lua')
end


-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
    state.Buff.Hasso = buffactive.Hasso or false
    state.Buff.Seigan = buffactive.Seigan or false
    state.Buff.Sekkanoki = buffactive.Sekkanoki or false
    state.Buff.Sengikori = buffactive.Sengikori or false
    state.Buff['Meikyo Shisui'] = buffactive['Meikyo Shisui'] or false
end

-------------------------------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file.
-------------------------------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.
function user_setup()
    state.OffenseMode:options('Normal', 'Acc', 'CP')
    state.HybridMode:options('Normal', 'PDT', 'Reraise')
    state.WeaponskillMode:options('Normal', 'Acc', 'Mod')
    state.PhysicalDefenseMode:options('Normal', 'PDT', 'Reraise')

    update_combat_form()

    -- Additional local binds
    send_command('bind ^` input /ja "Hasso" <me>')
    send_command('bind !` input /ja "Seigan" <me>')

    select_default_macro_book()
end


-- Called when this job file is unloaded (eg: job change)
function user_unload()
    send_command('unbind ^`')
    send_command('unbind !-')
end


-- Define sets and vars used by this job file.
function init_gear_sets()
    --------------------------------------
    -- Start defining the sets
    --------------------------------------

    -- Precast Sets
    -- Precast sets to enhance JAs
    sets.precast.JA.Berserk = {back="Cichol's Mantle",feet="Warrior's Calligae"}
    sets.precast.JA['Warcry'] = {head="Warrior's Mask"}
    sets.precast.JA['Blood Rage'] = {body="Ravager's Lorica +2"}
    sets.precast.JA['Restraint'] = {hands="Ravager's Mufflers +2"}


    -- Weaponskill sets
    -- Default set for any weaponskill that isn't any more specifically defined
    sets.precast.WS = {ammo="Ravager's Orb",
        head="Yaoyotl Helm",neck="Asperity Necklace",ear1="Brutal Earring",ear2="Moonshade Earring",
        body="Phorcys Korazin",hands="Otronif Gloves",ring1="Rajas Ring",ring2="Rufescent Ring",
        back="Aptitude Mantle",waist="Caudata Belt",legs="Sulevia's Cuisses +1",feet="Karieyh Sollerets +1"}
    sets.precast.WS.Acc = set_combine(sets.precast.WS, {back="Letalis Mantle"})

    -- Specific weaponskill sets.  Uses the base set if an appropriate WSMod version isn't found.
    sets.precast.WS['Upheaval'] = set_combine(sets.precast.WS, {neck="Flame Gorget",waist="Flame Belt",
		ring2="Terrasoul Ring"})


    -- Midcast Sets
    sets.midcast.FastRecast = {
        head="Yaoyotl Helm",
        body="Otronif Harness +1",hands="Otronif Gloves",
        legs="Phorcys Dirs",feet="Otronif Boots +1"}


    -- Sets to return to when not performing an action.

    -- Resting sets
    sets.resting = {neck="Wiglen Gorget",ring1="Sheltered Ring",ring2="Paguroidea Ring"}


    -- Idle sets (default idle set not needed since the other three are defined, but leaving for testing purposes)
    sets.idle.Town = {main="Tsurumaru", sub="Pole Grip",ammo="Ravager's Orb",
        head="Yaoyotl Helm",neck="Asperity Necklace",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Otronif Harness +1",hands="Otronif Gloves",ring1="Sheltered Ring",ring2="Paguroidea Ring",
        back="Aptitude Mantle",waist="Flume Belt",legs="Crimson Cuisses",feet="Hermes' Sandals"}

    sets.idle.Field = {
        head="Yaoyotl Helm",neck="Wiglen Gorget",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Otronif Harness +1",hands="Otronif Gloves",ring1="Sheltered Ring",ring2="Paguroidea Ring",
        back="Shadow Mantle",waist="Flume Belt",legs="Crimson Cuisses",feet="Hermes' Sandals"}

    sets.idle.Weak = {
        head="Twilight Helm",neck="Wiglen Gorget",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Twilight Mail",hands="Buremte Gloves",ring1="Sheltered Ring",ring2="Paguroidea Ring",
        back="Shadow Mantle",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Hermes' Sandals"}

    -- Defense sets
    sets.defense.PDT = {ammo="Iron Gobbet",
        head="Yaoyotl Helm",neck="Twilight Torque",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Otronif Harness +1",hands="Otronif Gloves",ring1="Defending Ring",ring2=gear.DarkRing.physical,
        back="Shadow Mantle",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Otronif Boots +1"}

    sets.defense.Reraise = {
        head="Twilight Helm",neck="Twilight Torque",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Twilight Mail",hands="Buremte Gloves",ring1="Defending Ring",ring2="Paguroidea Ring",
        back="Shadow Mantle",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Otronif Boots +1"}

    sets.defense.MDT = {ammo="Demonry Stone",
        head="Yaoyotl Helm",neck="Twilight Torque",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Otronif Harness +1",hands="Otronif Gloves",ring1="Defending Ring",ring2="Shadow Ring",
        back="Engulfer Cape",waist="Flume Belt",legs="Sulevia's Cuisses +1",feet="Otronif Boots +1"}

    sets.Kiting = {feet="Hermes' Sandals"}

    sets.Reraise = {head="Twilight Helm",body="Twilight Mail"}

    -- Engaged sets

    -- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
    -- sets if more refined versions aren't defined.
    -- If you create a set with both offense and defense modes, the offense mode should be first.
    -- EG: sets.engaged.Dagger.Accuracy.Evasion

    -- Normal melee group
    -- Delay 450 GK, 25 Save TP => 65 Store TP for a 5-hit (25 Store TP in gear)
    sets.engaged = {sub="Nepenthe Grip",ammo="Ravager's Orb",
        head="Flamma Zucchetto",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +1",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Cichol's Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.Acc = {sub="Tzacab Grip",ammo="Ravager's Orb",
        head="Flamma Zucchetto",neck="Lissome Necklace",ear1="Mache Earring",ear2="Mache Earring",
        body="Sulevia's Platemail +1",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ambuscade Ring",
        back="Sokolski Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.PDT = {ammo="Ravager's Orb",
        head="Felistris Mask",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +1",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.Acc.PDT = {ammo="Honed Tathlum",
        head="Felistris Mask",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +1",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.Reraise = {ammo="Ravager's Orb",
        head="Twilight Helm",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Twilight Mail",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.Acc.Reraise = {ammo="Ravager's Orb",
        head="Felistris Mask",neck="Lissome Necklace",ear1="Ethereal Earring",ear2="Brutal Earring",
        body="Sulevia's Platemail +1",hands="Flamma Manopolas +1",ring1="Rajas Ring",ring2="Ulthalam's Ring",
        back="Aptitude Mantle",waist="Ioskeha Belt",legs="Sulevia's Cuisses +1",feet="Sulevia's Leggings"}
    sets.engaged.CP = set_combine(sets.engaged, {back="Aptitude Mantle"})

    sets.buff.Sekkanoki = {hands="Unkai Kote +2"}
    sets.buff.Sengikori = {feet="Unkai Sune-ate +2"}
    sets.buff['Meikyo Shisui'] = {feet="Sakonji Sune-ate"}
end


-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for standard casting events.
-------------------------------------------------------------------------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic target handling to be done.
function job_pretarget(spell, action, spellMap, eventArgs)
    if spell.type == 'WeaponSkill' then
        -- Change any GK weaponskills to polearm weaponskill if we're using a polearm.
        if player.equipment.main=='Quint Spear' or player.equipment.main=='Quint Spear' then
            if spell.english:startswith("Tachi:") then
                send_command('@input /ws "Penta Thrust" '..spell.target.raw)
                eventArgs.cancel = true
            end
        end
    end
end

-- Run after the default precast() is done.
-- eventArgs is the same one used in job_precast, in case information needs to be persisted.
function job_post_precast(spell, action, spellMap, eventArgs)
    if spell.type:lower() == 'weaponskill' then
        if state.Buff.Sekkanoki then
            equip(sets.buff.Sekkanoki)
        end
        if state.Buff.Sengikori then
            equip(sets.buff.Sengikori)
        end
        if state.Buff['Meikyo Shisui'] then
            equip(sets.buff['Meikyo Shisui'])
        end
    end
end


-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
    -- Effectively lock these items in place.
    if state.HybridMode.value == 'Reraise' or
        (state.DefenseMode.value == 'Physical' and state.PhysicalDefenseMode.value == 'Reraise') then
        equip(sets.Reraise)
    end
end

-------------------------------------------------------------------------------------------------------------------
-- User code that supplements standard library decisions.
-------------------------------------------------------------------------------------------------------------------

-- Called by the 'update' self-command, for common needs.
-- Set eventArgs.handled to true if we don't want automatic equipping of gear.
function job_update(cmdParams, eventArgs)
    update_combat_form()
end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)

end

-------------------------------------------------------------------------------------------------------------------
-- Utility functions specific to this job.
-------------------------------------------------------------------------------------------------------------------

function update_combat_form()
    if areas.Adoulin:contains(world.area) and buffactive.ionis then
        state.CombatForm:set('Adoulin')
    else
        state.CombatForm:reset()
    end
end

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
    -- Default macro set/book
    if player.sub_job == 'WAR' then
        set_macro_page(1, 11)
    elseif player.sub_job == 'DNC' then
        set_macro_page(2, 11)
    elseif player.sub_job == 'THF' then
        set_macro_page(3, 11)
    elseif player.sub_job == 'NIN' then
        set_macro_page(4, 11)
    else
        set_macro_page(1, 11)
    end
end
