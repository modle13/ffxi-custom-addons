-------------------------------------------------------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified.
-------------------------------------------------------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
    mote_include_version = 2

    -- Load and initialize the include file.
    include('Mote-Include.lua')
end


-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
    state.Buff.Saboteur = buffactive.saboteur or false
    include('Mote-TreasureHunter')
    send_command('bind ^= gs c cycle treasuremode')
end

-------------------------------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file.
-------------------------------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()
    state.OffenseMode:options('None', 'Normal')
    state.HybridMode:options('Normal', 'PhysicalDef', 'MagicalDef')
    state.CastingMode:options('Normal', 'Resistant')
    state.IdleMode:options('Normal', 'PDT', 'MDT')

    gear.default.obi_waist = "Sekhmet Corset"

    select_default_macro_book()
end


-- Define sets and vars used by this job file.
function init_gear_sets()
    --------------------------------------
    -- Start defining the sets
    --------------------------------------
    sets.TreasureHunter = {head="White Rarab Cap +1", waist="Tarutaru Sash"}

    -- Precast Sets

    -- Precast sets to enhance JAs
    sets.precast.JA['Chainspell'] = {body="Vitiation Tabard"}


    -- Waltz set (chr and vit)
    sets.precast.Waltz = {
        head="Atrophy Chapeau +1",
        body="Atrophy Tabard +1",hands="Yaoyotl Gloves",
        back="Refraction Cape",legs="Hagondes Pants",feet="Hagondes Sabots"
    }

    -- Don't need any special gear for Healing Waltz.
    sets.precast.Waltz['Healing Waltz'] = {}

    -- Fast cast sets for spells

    -- 80% Fast Cast (including trait) for all spells, plus 5% quick cast
    -- No other FC sets necessary.
    sets.precast.FC = {
        ammo="Impatiens",
        head={ name="Merlinic Hood", augments={'"Fast Cast"+4','Mag. Acc.+12',}},
        neck="Voltsurge Torque",
        ear1="Malignance Earring",ear2="Loquacious Earring",
        body="Vitiation Tabard",hands="Leyline Gloves",ring1="Veneficium Ring",ring2="Lebeche Ring",
        back="Swith Cape",waist="Embla Sash",legs="Orvail Pants +1",
        feet={ name="Merlinic Crackows", augments={'"Fast Cast"+3','CHR+10',}},
    }

    sets.precast.FC.Impact = set_combine(sets.precast.FC, {head=empty,body="Twilight Cloak"})

    sets.precast.FC.Cure = set_combine(sets.precast.FC, {
        body={ name="Chironic Doublet", augments={'"Cure" spellcasting time -9%','VIT+12','Mag. Acc.+20 "Mag.Atk.Bns."+20',}},
    })

    -- Weaponskill sets
    -- Default set for any weaponskill that isn't any more specifically defined
    sets.precast.WS = {
        head="Jhakri Coronal +1",neck="Lissome Necklace",ear1="Ishvara Earring",ear2="Moonshade Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +2",ring1="Rajas Ring",ring2="Excelsis Ring",
        back={ name="Sucellos's Cape", augments={'STR+5','Weapon skill damage +10%',}},
        waist="Caudata Belt",legs="Jhakri Slops +1",
        feet="Jhakri Pigaches +1",
    }

    sets.precast.WS['Savage Blade'] = set_combine(sets.precast.WS, {
        neck="Thunder Gorget",
        waist="Thunder Belt"
    })

    sets.precast.WS['Seraph Blade'] = set_combine(sets.precast.WS, {
        head="Welkin Crown",
        -- neck="Sanctity Necklace",
        neck="Light Gorget",
        ear1="Malignance Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +2",ring1="Fortified Ring",ring2="Jhakri Ring",
        back="Toro Cape",
        -- waist="Chiner's Belt +1",
        waist="Aswang Sash",
        legs="Jhakri Slops +1"
    })

    -- Specific weaponskill sets.  Uses the base set if an appropriate WSMod version isn't found.
    sets.precast.WS['Requiescat'] = set_combine(sets.precast.WS, {
        neck="Soil Gorget",ear1="Brutal Earring",ear2="Moonshade Earring",
        ring1="Aquasoul Ring",ring2="Aquasoul Ring",waist="Soil Belt"
    })

    sets.precast.WS['Sanguine Blade'] = {ammo="Witchstone",
        head="Hagondes Hat",neck="Eddy Necklace",ear1="Friomisi Earring",ear2="Hecate's Earring",
        body="Hagondes Coat",hands="Yaoyotl Gloves",ring1="Strendu Ring",ring2="Acumen Ring",
        back="Toro Cape",legs="Hagondes Pants"
    }

    sets.precast.WS['Aeolian Edge'] = {
        head="Welkin Crown",neck="Sanctity Necklace",ear1="Malignance Earring",ear2="Friomisi Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +2",ring1="Fortified Ring",ring2="Jhakri Ring",
        back="Toro Cape",waist="Chiner's Belt +1",legs="Jhakri Slops +1"
    }

    -- Midcast Sets

    sets.midcast.FastRecast = {
        head="Atrophy Chapeau +1",ear2="Loquacious Earring",
        body="Vitiation Tabard",hands="Gendewitha Gages",ring1="Prolix Ring",
        back="Swith Cape +1",waist="Witful Belt",legs="Hagondes Pants",feet="Hagondes Sabots"
    }

    sets.midcast.Cure = {
        main="Daybreak",sub="Sors Shield",
        head="Vanya Hood",neck="Phalaina Locket",ear1="Mendicant's Earring",ear2="Loquacious Earring",
        body={ name="Chironic Doublet", augments={'"Cure" spellcasting time -9%','VIT+12','Mag. Acc.+20 "Mag.Atk.Bns."+20',}},
        hands="Bokwus Gloves",
        ring1="Ephedra Ring",ring2="Lebeche Ring",
        back="Ghostfyre Cape",waist="Austerity Belt +1",legs="Carmine Cuisses +1",feet="Psycloth Boots"
    }

    sets.midcast.Curaga = sets.midcast.Cure
    sets.midcast.CureSelf = {ring1="Kunaji Ring",ring2="Asklepian Ring"}

    sets.midcast.Cursna = {main=my_main,sub=my_sub,
        neck="Malison Medallion",ear1="Healing Earring",
        body="Vitiation Tabard",ring1="Ephedra Ring"
    }

    sets.midcast['Enhancing Magic'] = {
        head="Befouled Crown",
        ear1="Mimir Earring",
        back="Sucellos's Cape",
        waist="Embla Sash",
        legs={ name="Carmine Cuisses +1", augments={'HP+80','STR+12','INT+12',}},
    }

    sets.midcast.Phalanx = {
        feet={ name="Chironic Slippers", augments={'Accuracy+11','Pet: AGI+10','Phalanx +5',}},
    }

    sets.midcast.Refresh = {legs="Lethargy Fuseau +1",waist="Gishdubar Sash"}

    sets.midcast.Stoneskin = {waist="Siegel Sash"}

    sets.midcast['Enfeebling Magic'] = {
        main="Daybreak",sub="Genmei Shield",
        ammo="Kalboron Stone",
        head="Jhakri Coronal +1",neck="Duelist's Torque",ear1="Malignance Earring",ear2="Vor Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +2",ring1="Stikini Ring",ring2="Stikini Ring",
        back="Sucellos's Cape",waist="Ovate Rope",legs="Jhakri Slops +1",feet="Malignance Boots"
    }

    sets.midcast['Dia III'] = set_combine(sets.midcast['Enfeebling Magic'], {head="Vitiation Chapeau"})

    sets.midcast['Slow II'] = set_combine(sets.midcast['Enfeebling Magic'], {head="Vitiation Chapeau"})

    sets.midcast['Elemental Magic'] = {
        -- main="Lehbrailg +2",sub="Zuuxowu Grip",ammo="Dosis Tathlum",
        head="Jhakri Coronal +1",neck="Mizukage-no-Kubikazari",ear1="Friomisi Earring",ear2="Hecate's Earring",
        body="Jhakri Robe +1",hands="Jhakri Cuffs +2",ring1="Strendu Ring",ring2="Locus Ring",
        back="Izdubar Mantle",waist=gear.ElementalObi,legs="Jhakri Slops +1",feet="Jhakri Pigaches +1"
    }

    sets.midcast.Impact = set_combine(sets.midcast['Elemental Magic'], {head=empty,body="Twilight Cloak"})

    sets.midcast['Dark Magic'] = {
        -- main="Lehbrailg +2",sub="Mephitis Grip",ammo="Kalboron Stone",
        head="Atrophy Chapeau +1",neck="Henic Torque",ear1="Lifestorm Earring",ear2="Psystorm Earring",
        body="Vanir Cotehardie",hands="Gendewitha Gages",ring1="Prolix Ring",ring2="Sangoma Ring",
        back="Refraction Cape",waist="Goading Belt",legs="Bokwus Slops",feet="Bokwus Boots"
    }

    --sets.midcast.Stun = set_combine(sets.midcast['Dark Magic'], {})

    sets.midcast.Drain = set_combine(sets.midcast['Dark Magic'], {ring1="Excelsis Ring", waist="Fucho-no-Obi"})

    sets.midcast.Aspir = sets.midcast.Drain


    -- Sets for special buff conditions on spells.

    sets.midcast.EnhancingDuration = set_combine(sets.midcast['Enhancing Magic'], {
        hands="Atrophy Gloves +1",back="Sucellos's Cape",waist="Embla Sash",feet="Lethargy Houseaux +1"
    })

    sets.buff.ComposureOther = set_combine(sets.midcast.EnhancingDuration, {
        head="Lethargy Chappel +1",
        body="Lethargy Sayon +1",hands="Lethargy Gantherots +1",
        legs="Lethargy Fuseau +1",feet="Lethargy Houseaux +1"
    })

    sets.buff.Saboteur = {hands="Lethargy Gantherots +1"}

    -- Sets to return to when not performing an action.

    -- Resting sets
    sets.resting = {
        -- main="Chatoyant Staff",
        head="Vitiation Chapeau",neck="Wiglen Gorget",
        body="Atrophy Tabard +1",hands="Serpentes Cuffs",ring1="Sheltered Ring",ring2="Paguroidea Ring",
        waist="Austerity Belt",legs="Nares Trews",feet="Chelona Boots +1"
    }

    -- Idle sets
    sets.idle = {
        main="Daybreak",sub="Genmei Shield",
        -- main="Earth Staff",sub="Mensch Strap",
        ammo="Staunch Tathlum",
        -- ammo="Homiliary",
        head="Ayanmo Zucchetto +1",
        -- head="Malignance Chapeau",
        neck="Sanctity Necklace",
        ear1="Eabani Earring",ear2="Cryptic Earring",
        -- body="Ayanmo Corazza +2",
        body="Jhakri Robe +1",
        hands="Ayanmo Manopolas +1",
        ring1="Etana Ring",ring2="Ayanmo Ring",
        back="Solemnity Cape",waist="Flume Belt",legs="Carmine Cuisses +1",feet="Ayanmo Gambieras +1"
    }

    sets.idle.Town = set_combine(sets.idle, {})

    -- sets.idle.Weak = {
    --     -- main="Bolelabunga",sub="Genmei Shield",
    --     ammo="Impatiens",
    --     head="Vitiation Chapeau",neck="Wiglen Gorget",ear1="Bloodgem Earring",ear2="Loquacious Earring",
    --     body="Atrophy Tabard +1",hands="Serpentes Cuffs",ring1="Sheltered Ring",ring2="Paguroidea Ring",
    --     back="Solemnity Cape",waist="Flume Belt",legs="Carmine Cuisses +1",feet="Malignance Boots"}

    sets.idle.PDT = {
        -- main="Bolelabunga",sub="Genmei Shield",
        ammo="Staunch Tathlum",
        head="Gendewitha Caubeen +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Gendewitha Bliaut +1",hands="Gendewitha Gages",ring1="Defending Ring",ring2=gear.DarkRing.physical,
        back="Solemnity Cape",waist="Flume Belt",legs="Osmium Cuisses",feet="Gendewitha Galoshes"
    }

    sets.idle.MDT = {
        -- main="Bolelabunga",sub="Genmei Shield",
        ammo="Staunch Tathlum",
        head="Gendewitha Caubeen +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Gendewitha Caubeen +1",hands="Yaoyotl Gloves",ring1="Defending Ring",ring2="Shadow Ring",
        back="Engulfer Cape",waist="Flume Belt",legs="Osmium Cuisses",feet="Gendewitha Galoshes"
    }


    -- Defense sets
    sets.defense.PDT = {
        ammo="Staunch Tathlum",
        head="Atrophy Chapeau +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Hagondes Coat",hands="Gendewitha Gages",ring1="Defending Ring",ring2=gear.DarkRing.physical,
        back="Solemnity Cape",waist="Flume Belt",legs="Hagondes Pants",feet="Gendewitha Galoshes"
    }

    sets.defense.MDT = {
        ammo="Staunch Tathlum",
        head="Atrophy Chapeau +1",neck="Twilight Torque",ear1="Bloodgem Earring",ear2="Loquacious Earring",
        body="Atrophy Tabard +1",hands="Yaoyotl Gloves",ring1="Defending Ring",ring2="Shadow Ring",
        back="Engulfer Cape",waist="Flume Belt",legs="Bokwus Slops",feet="Gendewitha Galoshes"
    }

    sets.Kiting = {legs="Carmine Cuisses +1"}

    sets.latent_refresh = {ring1="Stikini Ring +1",waist="Fucho-no-obi"}

    -- Engaged sets1

    -- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
    -- sets if more refined versions aren't defined.
    -- If you create a set with both offense and defense modes, the offense mode should be first.
    -- EG: sets.engaged.Dagger.Accuracy.Evasion

    -- Normal melee group
    sets.engaged = {
        main="Naegling",sub="Odium",
        ammo="Amar Cluster",
        -- ammo="Focal Orb",
        -- ammo="Staunch Tathlum",
        head="Ayanmo Zucchetto +1",neck="Clotharius Torque",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Ayanmo Corazza +2",
        hands="Ayanmo Manopolas +1",
        ring1="Rajas Ring",ring2="Begrudging Ring",
        back="Atheling Mantle",waist="Sailfi Belt +1",legs="Ayanmo Cosciales +1",feet="Ayanmo Gambieras +1"
    }

    sets.engaged.Defense = {ammo="Demonry Stone",
        head="Malignance Chapeau",neck="Asperity Necklace",ear1="Bladeborn Earring",ear2="Steelflash Earring",
        body="Atrophy Tabard +1",hands="Atrophy Gloves +1",ring1="Rajas Ring",ring2="K'ayres Ring",
        back="Kayapa Cape",waist="Goading Belt",legs="Osmium Cuisses",feet="Malignance Boots"
    }

end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for standard casting events.
-------------------------------------------------------------------------------------------------------------------

-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
    if spell.skill == 'Enfeebling Magic' and state.Buff.Saboteur then
        equip(sets.buff.Saboteur)
    elseif spell.skill == 'Enhancing Magic' then
        equip(sets.midcast.EnhancingDuration)
        if buffactive.composure and spell.target.type == 'PLAYER' then
            equip(sets.buff.ComposureOther)
        end
    elseif spellMap == 'Cure' and spell.target.type == 'SELF' then
        equip(sets.midcast.CureSelf)
    end
end

-------------------------------------------------------------------------------------------------------------------
-- Job-specific hooks for non-casting events.
-------------------------------------------------------------------------------------------------------------------

-- Handle notifications of general user state change.
function job_state_change(stateField, newValue, oldValue)
    if stateField == 'Offense Mode' then
        if newValue == 'None' then
            enable('main','sub','range')
        else
            disable('main','sub','range')
        end
    end
end

-------------------------------------------------------------------------------------------------------------------
-- User code that supplements standard library decisions.
-------------------------------------------------------------------------------------------------------------------

-- Modify the default idle set after it was constructed.
function customize_idle_set(idleSet)
    if player.mpp < 51 then
        idleSet = set_combine(idleSet, sets.latent_refresh)
    end

    return idleSet
end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)
    display_current_caster_state()
    eventArgs.handled = true
end

-------------------------------------------------------------------------------------------------------------------
-- Utility functions specific to this job.
-------------------------------------------------------------------------------------------------------------------

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
    -- Default macro set/book
    if player.sub_job == 'DNC' then
        set_macro_page(2, 10)
    elseif player.sub_job == 'NIN' then
        set_macro_page(3, 10)
    elseif player.sub_job == 'THF' then
        set_macro_page(4, 10)
    else
        set_macro_page(1, 10)
    end
end
