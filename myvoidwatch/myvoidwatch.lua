_addon.name = 'MyInventoryWatch'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'myvw'}

require('logger')
res = require('resources')
-- luau contains the config loader
require('luau')

-- to reset any endless recursions :/
reload_time = 300
start_time = os.time()

settings = config.load(defaults)

-- local target_id = 17309989
-- local target_name = "Aello"
-- local target_id = 16793873
-- local target_id = 16793862
-- local target_name = "Bismarck"
-- local target_id = 17506681 -- south, up a floor
local target_id = 17506676 -- north
local target_name = "Qilin"
-- local target_id = 17167113 -- impatiens
-- local target_name = "Pancimanci"
local ws_sleep_time = 3

local active_check_time = 0
local restart_voidwatch_threshold = 50
local skip_restart = false

local global_trigger_delay = 1
local ws_delay = 3
local engage_delay = 2
local item_delay = 15
local ability_delay = 45
local mob_check_delay = 1

local last_global_trigger = 0
local last_ws_trigger = 0
local last_engage_trigger = 0
local last_item_trigger = 0
local last_abil_trigger = 0
local last_mob_check_trigger = 0

local weapon_skill = "Evisceration"
local tp_threshold = 1000
local job = windower.ffxi.get_player().main_job
local player_name = windower.ffxi.get_player().name
-- local temp_items = {"Dusty Wing", "Stalwart's Tonic"} --, "Braver's Drink", "Champion's Tonic"}
local temp_items = {"Dusty Wing"} --, "Braver's Drink", "Champion's Tonic"}

local abilities = {["Meditate"] = "me", ["Hasso"] = "me", ["Berserk"] = "me", ["Warcry"] = "me",}
local job_abil = {
    ["SAM"] = {["Meditate"] = "me", ["Hasso"] = "me", ["Berserk"] = "me", ["Warcry"] = "me",},
    ["DRG"] = {["Angon"] = "t", ["Meditate"] = "me", ["Hasso"] = "me", ["Jump"] = "t", ["High Jump"] = "t", ["Soul Jump"] = "t", ["Spirit Jump"] = "t",},
    ["THF"] = {["Berserk"] = "me", ["Warcry"] = "me"},
    ["BRD"] = {["Valor Minuet IV"] = "me"},
    ["COR"] = {["Chaos Roll"] = "me", ["Samurai Roll"] = "me"},
    ["RDM"] = {["Composure"] = "me", ["Haste II"] = "me", ["Dia III"] = "t", ["Temper"] = "me", ["Enblizzard"] = "me",},
}

local abil_buff_map = {
    ["Haste II"] = "Haste",
}

-- local weaponskills = {
--     -- ["Chihemihe"] = "Savage Blade",
--     -- ["Chihemihe"] = "Impulse Drive",
--     -- ["Chihemihe"] = "Rudra's Storm",
--     -- ["Chihemihe"] = "Evisceration",
--     ["Lauralanthalesa"] = "Vorpal Blade",
--     ["Cheeheemeehee"] = "Savage Blade",
--     ["Tikamajere"] = "Tachi Fudo",
--     -- ["Tikamajere"] = "Tachi Shoha",
--     -- ["Crowmane"] = "Tachi Shoha",
--     ["Kitiaramatar"] = "Savage Blade",
--     -- ["Kitiaramatar"] = "Rudra's Storm",
--     -- ["Kitiaramatar"] = "Evisceration",
--     ["Crowmane"] = "Savage Blade",
--     ["Subprocess"] = "Savage Blade",
--     ["Turtleburglar"] = "Savage Blade",
--     ["Yamburglar"] = "Savage Blade",
--     ["Boatmeal"] = "Savage Blade",
--     ["Undresatndalbe"] = "Savage Blade",
--     ["Maptakenback"] = "Vorpal Blade",
-- }

local tp_thresholds = {
    ["Savage Blade"] = 750,
    ["Vorpal Blade"] = 1000,
    ["Impulse Drive"] = 1750,
    ["Evisceration"] = 1000,
    ["Rudra's Storm"] = 1500,
    ["Tachi Shoha"] = 1000,
    ["Tachi Fudo"] = 1000,
}

local conditions = {
    fighting = false,
    engaged = false,
    mob_present = false,
}

-----------------------------
-- windower event triggers --
-----------------------------

windower.register_event('incoming text', function(original, new, color)
    local now = os.time()

    original = original:strip_format()

    run_check(original)

    if not skip_restart and now - active_check_time > restart_voidwatch_threshold and now - last_mob_check_trigger > mob_check_delay then
        log("been a while since vw happened, trying again")
        active_check_time = now
        last_mob_check_trigger = now
        windower.send_command('input //lua unload voidwatch')
        coroutine.sleep(1)
        windower.send_command('input //lua load voidwatch')
        coroutine.sleep(1)
        windower.send_command('input //vw start')
        do return end
    end

    if not conditions["mob_present"] and now - last_mob_check_trigger > mob_check_delay then
        last_mob_check_trigger = now
        if check_for_target() then
            log("found target")
            log("starting")
            conditions["mob_present"] = true
            active_check_time = now
        end
    end

    -- end check
    if string.match(original, "Final Spectral Alignment") or string.match(original, "defeats " .. target_name) then
        reset_conditions()
        active_check_time = now
        log("done")
    end

    -- start check
    if conditions["mob_present"] and not conditions["engaged"] and now - last_engage_trigger > engage_delay then
        log('engage check triggered')
        last_engage_trigger = now
        execute_weaponskill()
        engage()
    end

    -- engagement check
    if string.match(original, "hits " .. target_name .. " for") and now - last_ws_trigger > ws_delay then
        active_check_time = now
        conditions["engaged"] = true
        log("engaged; job: " .. job .. ", ws: " .. weapon_skill .. ", tp_threshold: " .. tp_threshold)
        conditions["fighting"] = true
        windower.ffxi.run(false)
        check_weaponskill()
    end

    if now - last_item_trigger > item_delay and not tp_threshold_met then
        log('item check triggered')
        last_item_trigger = now
        use_items()
    end

    if conditions["engaged"] and now - last_abil_trigger > ability_delay and not tp_threshold_met then
        log('ability check triggered')
        last_abil_trigger = now
        execute_abilities()
    end

    local tp_threshold_met = windower.ffxi.get_player().vitals.tp >= tp_threshold

    if now - last_global_trigger > global_trigger_delay then
        last_global_trigger = now
        -- log('continuing')
    else
        -- exit
        do return end
    end
    -- log('continued')

    if conditions["engaged"] and now - last_item_trigger > item_delay and not tp_threshold_met then
        log('item check triggered')
        last_item_trigger = now
        use_items()
    end

    if conditions["engaged"] and now - last_abil_trigger > ability_delay and not tp_threshold_met then
        log('ability check triggered')
        last_abil_trigger = now
        execute_abilities()
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    if command1 == 'ws' then
        -- joins remainder of table with space
        weapon_skill = table.concat({...}, " ")
        log('ws set to: ' .. tostring(weapon_skill))
    elseif command1 == 'restart' then
        skip_restart = not skip_restart
        log('setting skip_restart: ' .. tostring(skip_restart))
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)


-----------------------------
--        functions        --
-----------------------------

function run_check(compare)
    if string.match(compare, "Unable to see") or string.match(compare, "out of range") then
        windower.ffxi.run()
        log("running, ugh")
    end
end

function reset_conditions()
    for k,v in pairs(conditions) do
        conditions[k] = false
    end
    log('no longer fighting')
    windower.ffxi.run(false)
end

-- this will trigger when VW starts, and when player first engages
-- this means the loop may trigger twice
function check_weaponskill()
    if conditions["fighting"] then
        if windower.ffxi.get_player().vitals.tp >= tp_threshold then
            log('executing weapon skill')
            execute_weaponskill()
        end
        last_ws_trigger = os.time()
    end
end

function check_for_target()
    log('checking for ' .. target_name)
    local target = windower.ffxi.get_mob_by_id(target_id)
    local valid_target = false
    if target ~= nil then
        valid_target = target.hpp > 0
    end
    return valid_target
end

function engage()
    log('engaging')
    abilities = job_abil[job]
    weapon_skill = settings.weaponskill
    -- if weapon_skill == nil then
    --     weapon_skill = weaponskills[player_name]
    -- end
    tp_threshold = tp_thresholds[weapon_skill]
    windower.send_command('input //settarget ' .. target_id)
    windower.send_command('input /attack <t>')
end

function execute_weaponskill()
    log('executing weapon skill')
    windower.send_command('input /ws "' .. weapon_skill .. '" ' .. target_name)
end

function use_items()
    log('using items')
    local inventory = windower.ffxi.get_items(0)
    -- TODO: detect whether item is in inventory
    if (windower.ffxi.get_player().vitals.tp) < 1000 and conditions["fighting"] then
        item_command = 'input /item "Dusty Wing" <me>'
        windower.send_command(item_command)
    end
    for k,v in pairs(temp_items) do
        item_command = 'input /item "' .. v .. '" <me>'
        windower.send_command(item_command)
    end
end

function execute_abilities()
    log('executing abilities')
    for k,v in pairs(abilities) do
        -- check buff presence
        windower.send_command('input /ja "' .. k .. '" <' .. v .. '>')
        coroutine.sleep(3)
        log('using ' .. k)
    end
end

function has_buff_by_name(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    local target_buff_id = res.buffs:with('name', target_buff).id

    log('checking for ' .. target_buff .. ' with target id ' .. target_buff_id)

    for k,v in pairs(player_buffs) do
        if v == tonumber(target_buff_id) then
            buff_active = true
            log('buff already active : ' .. target_buff)
            break
        end
    end
    if not buff_active then
        log('BUFF NOT ACTIVE : ' .. target_buff)
    end
    return buff_active
end
