_addon.name = 'weapontrial'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'trial'}

require('logger')

local chat = windower.add_to_chat

-- to reset any endless recursions :/

local abilities = {["Meditate"] = "me", ["Defender"] = "me", ["Hasso"] = "me"}
-- update this dynamically, run it every cycle
local ready_abilities = T{["Defender"] = "me"}

local run = false
local ws = "Brainshaker"
local sleep_time = 1
local engaged = false
local hpp_threshold = 20
local sane_threshold = 900

log("starting script")

function run_it()
    if ws == nil then
        log("ws is not set")
        return
    end
    running = true
    log("running")
    while running do
        process_abilities()
        if can_ws() then
            process_ws()
        end
        coroutine.sleep(sleep_time)
    end
end

function stop_it()
    running = false
    log("stopping")
end

function can_ws()
    check_combat()
    if not engaged then
        return false
    end
    return windower.ffxi.get_player().vitals.tp > 1000
end

function check_combat()
    local player = windower.ffxi.get_player()
    if not player.in_combat then
        disengage()
    end
end

function disengage()
    if engaged then
        log("disengaged")
    end
    engaged = false
end

function process_ws()
    windower.send_command('input /ws "' .. ws .. '" <t>')
end

function process_abilities()
    function use_abilities()
        for k,v in pairs(ready_abilities) do
            windower.send_command('input /ja "' .. k .. '" <' .. v .. '>')
            coroutine.sleep(sleep_time)
        end
    end
end


windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = {...}
    if command1 == 'run' or command1 == 'start' then
        run_it()
    elseif command1 == 'stop' then
        stop_it()
    elseif command1 == 'ws' then
        ws_input = table.concat(cmd_remainder, ' ')
        if ws_input == 'get' or ws_input == 'show' then
            chat(122,'Selected WS: ' .. ws .. '.')
        else
            ws = ws_input
            chat(122,'WS set to: ' .. ws .. '.')
        end
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    if string.match(original, windower.ffxi.get_player().name .. " hits the") and not engaged then
        log("engaged")
        engaged = true
    elseif string.match(original, "is out of range") and engaged then
        disengage()
    elseif string.match(original, "Unable to see the") and engaged then
        disengage()
    end
end)

run_it()
