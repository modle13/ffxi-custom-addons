_addon.name = 'Myscript'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'doit'}

require('logger')

-- to reset any endless recursions :/
reload_time = 300
start_time = os.time()

local target_ids = {17969868, 17961638}

-- (innin) 421, (berserk) 56, (aggressor) 58
local start_message = "You are now participating in a Domain Invasion"
local abilities = {["Innin"] = 421, ["Aggressor"] = 56, ["Berserk"] = 58}
local mob_target_id = 0
local mob_target_name = ""
local elvorseal_buff_id = 603

local conditions = {
    fighting = false,
    engaged = false,
    doing_things = false,
    have_elvorseal = false,
}

function reset_conditions()
    for k,v in pairs(conditions) do
        conditions[k] = false
    end
end

function get_buffs()
    local player = windower.ffxi.get_player()
    return player["buffs"]
end

function check_elvorseal()
    if have_buff(elvorseal_buff_id) then
        conditions["have_elvorseal"] = true
        log("got the buff")
    end
end

function have_buff(id)
    local current_buffs = get_buffs()
    for k,v in pairs(buffs) do
        if v == id then
            return true
        end
    end
    return false
end

function engage()
    windower.send_command('input //settarget ' .. mob_target_id)
    windower.send_command('input /attack <t>')
    if conditions["fighting"] and not conditions["engaged"] then
        coroutine.schedule(engage, 1)
    end
end

function use_abilities()
    for k,v in pairs(abilities) do
        if not have_buff(v) then
            windower.send_command('input /ja "' .. k .. '" <me>')
        end
        coroutine.sleep(1)
    end
    coroutine.schedule(use_abilities, 10)
end

function do_things()
    -- if mob target hp = 0, reset target
    coroutine.schedule(engage, 1)
    coroutine.schedule(use_abilities, 2)
end

function get_mob()
    if not conditions["have_elvorseal"] then
        log ("no elvorseal yet")
        coroutine.schedule(get_mob, 3)
    end

    for k,v in pairs(target_ids) do
        local target = windower.ffxi.get_mob_by_id(v)
        if (target ~= nil and target.hpp > 0) then
            mob_target_id = v
            mob_target_name = target.name
        end
    end

    if (mob_target_id == 0) then
        coroutine.schedule(get_mob, 3)
    else
        coroutine.schedule(do_things, 1)
    end
end

function run_check(compare)
    if string.match(compare, "cannot see") then
        windower.ffxi.run()
        log("running, ugh")
    end
end

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    if os.time() - start_time > reload_time and not conditions["fighting"] then
        log("been a while; reloading script")
        windower.send_command("input //lua reload myscript")
    end

    -- run_check(original)
    if string.match(original, "hits " .. mob_target_name .. " for") and not conditions["engaged"] then
        log("engaged")
        conditions["engaged"] = true
        conditions["fighting"] = true
        coroutine.schedule(do_things, 1)
        windower.ffxi.run(false)
    end
    if string.match(original, start_message) then
        log("starting")
        conditions["fighting"] = true
        coroutine.schedule(do_things, 1)
    end
end)

get_mob()
