_addon.name = 'MyAutoBattle'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'myab'}

require('logger')

-- my modules
require('data')
require('settings')
require('targets')

resources = require('resources')

-- local get_items = windower.ffxi.get_items
-- local player_items = get_items()
-- for _,item in ipairs(player_items.equipment) do
--     if item.id ~= 0 then
--         local item_name = resources.items[tonumber(item.id)].name
--         log(item_name .. ' ' .. item.id)
--     end
-- end
-- log(table.concat(resources.items[tonumber(player_items.equipment)], ", "))

local current_target = nil

local abilities = {["Meditate"] = "me", ["Hasso"] = "me", ["Berserk"] = "me", ["Warcry"] = "me",}

local abilities = data.job_abil_map[settings.job]

local pull_ability = data.pull_abil_map[settings.job]
if not pull_ability then
    pull_ability = data.pull_abil_map[settings.support_job]
end
local weapon_skill = data.weaponskills[settings.player_name]
local tp_threshold = data.tp_thresholds[weapon_skill]
if tp_threshold == nil then
    tp_threshold = 1000
end

local conditions = {
    fighting    = false,
    engaged     = false,
    mob_present = false,
}

local camp = data.camps[settings.player_name]
local camp_list = targets.target_lists[camp]

-----------------------------
-- windower event triggers --
-----------------------------

windower.register_event('incoming text', function(original, new, color)
    -- this will trigger infinitely off of its own log messages; be careful
    -- log('triggering incoming text')
    local now = os.time()

    execute_getting_hit_check(now, original)
    execute_smacking_mob_check(now, original)

    -- add checks for hitting party members
    -- see mytenzen for getting party member table
    if not conditions["engaged"] and not conditions["fighting"] and (
        string.match(original, settings.player_name .. " scores a critical hit") or
        string.match(original, settings.player_name .. " hits the") or
        check_party_hits(original)
    ) then
        current_target = windower.ffxi.get_mob_by_target("t")
        settings.run_check_count = 0
        if current_target ~= nil then
            log_message("starting")
            conditions["mob_present"] = true
            engage()
        end
    end

    if current_target == nil then
        do return end
    end

    original = original:strip_format()

    if string.match(original, "You can only use that command during battle") then
        reset_conditions()
    end

    if string.match(original, "You cannot use that command while unconscious") then
        reset_conditions()
        settings.running = false
    end

    if string.match(original, "Your target is already claimed") then
        reset_conditions()
        settings.running = false
    end

    run_check(original)

    -- end check
    if current_target ~= nil and (
        string.match(original, "defeats " .. current_target.name) or
        string.match(original, "defeats the " .. current_target.name)
    ) then
        reset_conditions()
    end

    -- engagement check
    -- will engage and stop moving if have target and landed a hit
    if current_target ~= nil and (
        string.match(original, "scores a critical hit") or
        string.match(original, "hits " .. current_target.name .. " for") or
        string.match(original, "hits the " .. current_target.name .. " for")
    ) then
        settings.engage_check_count = 0
        conditions["fighting"] = true
        windower.ffxi.run(false)
        trigger_engaged_actions()
        -- ensures main trigger doesn't happen if being attacked already
        settings.last_main_trigger = now
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = table.concat({...}, ' ')
    if command1 == 'start' or command1 == 'run' then
        settings.running = true
        log('running operation')
        run()
    elseif command1 == 'distance' then
        converted_distance = tonumber(cmd_remainder)
        if converted_distance ~= nil then
            settings.check_distance = converted_distance
            log('distance set to ' .. tostring(settings.check_distance))
        else
            log('not setting distance; input is not a number: ' .. tostring(cmd_remainder))
        end
    elseif command1 == 'stop' then
        settings.running = false
        log('stopping operation')
    elseif command1 == 'pull' then
        settings.puller = true
        settings.killer = false
        log('set as settings.puller')
    elseif command1 == 'kill' then
        settings.puller = false
        settings.killer = true
        log('set as settings.killer')
    elseif command1 == 'reset' then
        reset_conditions()
    elseif command1 == 'camp' then
        camp = cmd_remainder
        camp_list = data.target_lists[camp]
        log('camp set to ' .. camp)
    elseif command1 == 'cond' then
        for k,v in pairs(conditions) do
            log(k .. ', ' .. tostring(v))
        end
    elseif command1 == 'skipws' then
        settings.skip_ws = not settings.skip_ws
        log('skip ws set to ' .. tostring(settings.skip_ws))
    elseif command1 == 'debug' then
        settings.debug = not settings.debug
        log('debug set to ' .. tostring(settings.debug))
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)

-----------------------------
-- main function           --
-----------------------------

function run()
    while settings.running do
        settings.char_claim_id = data.claim_ids[settings.player_name]
        main()
        coroutine.sleep(1)
    end
end

function main()
    local now = os.time()

    -- if current_target ~= nil and (current_target.claim_id ~= settings.char_claim_id or current_target.claim_id ~= 0) then
    --     -- reset_conditions()
    --     do return end
    -- end

    if now - settings.last_main_trigger < settings.main_trigger_delay then
        do return end
    end

    -- if food and not has_buff_by_name('Food') then
    --     windower.send_command('input /item "Red Curry Bun" <me>')
    --     log('Fooding')
    -- end

    if not has_buff_by_name('Composure') then
        windower.send_command('input /ma "Composure" <me>')
        log('Composing self')
        do return end
    end

    if not has_buff_by_name('Haste') then
        windower.send_command('input /ma "Haste II" <me>')
        log('Hasting')
        do return end
    end

    if not has_buff_by_name('Multi Strikes') then
        windower.send_command('input /ma "Temper" <me>')
        log('Tempering')
        do return end
    end

    -- this means targetting is messed up, so start over
    if settings.getting_beat and not settings.smacking_mob then
        reset_conditions()
        do return end
    end

    -- start check
    if current_target and current_target.hpp <= 0 then
        current_target = nil
    end

    -- 1 is engaged
    -- 0 is disengaged
    conditions["engaged"] = windower.ffxi.get_player().status == 1

    if current_target and conditions["mob_present"] and not conditions["engaged"] and now - settings.last_engage_trigger > settings.engage_delay then
        log_message('engage check triggered, target has hpp: ' .. current_target.hpp, 'debug')
        settings.engage_check_count = settings.engage_check_count + 1
        if settings.engage_check_count == settings.max_engage_check_count then
            log('too many engage checks, resetting target')
            reset_conditions()
            do return end
        end
        settings.last_engage_trigger = now
        engage()
        local message = "engaged; job: " .. settings.job .. ", ws: " .. weapon_skill .. ", tp_threshold: " .. tp_threshold .. ", char claim id : " .. settings.char_claim_id
        log_message(message, 'debug')
        if settings.puller then
            log('sending pull command')
            windower.send_command('input /ja "' .. pull_ability .. '" <t>')
        end
    end

    if not settings.getting_beat and not conditions["engaged"] and not conditions["mob_present"] and now - settings.last_mob_check_trigger > settings.mob_check_delay then
        log('checking for mob')
        settings.last_mob_check_trigger = now
        current_target = check_for_target()
        if current_target ~= nil then
            log_message("starting")
            engage()
            conditions["mob_present"] = true
        end
    end

    trigger_engaged_actions()
end

function trigger_engaged_actions()
    local now = os.time()

    if conditions["fighting"] and now - settings.last_ws_trigger > settings.ws_delay then
        settings.last_ws_trigger = now
        check_weaponskill()
    end

    local tp_threshold_met = windower.ffxi.get_player().vitals.tp >= tp_threshold

    if conditions["fighting"] and
        conditions["engaged"] and
        now - settings.last_abil_trigger > settings.ability_delay and not tp_threshold_met
    then
        log('ability check triggered')
        settings.last_abil_trigger = now
        execute_abilities()
    end
end


-----------------------------
--        functions        --
-----------------------------

function has_buff_by_name(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    -- log('checking for target buff ' .. target_buff)
    local target_buff_id = resources.buffs:with('name', target_buff).id

    for k,v in pairs(player_buffs) do
        if v == tonumber(target_buff_id) then
            buff_active = true
            break
        end
    end
    if not buff_active then
        log('BUFF NOT ACTIVE : ' .. target_buff)
    end
    return buff_active
end

function log_message(message, level)
    if level and level == 'debug' then
        if settings.debug then
            log(message)
        end
    else
        log(message)
    end
end

function execute_getting_hit_check(now, original_text)
    local getting_hit_check = check_party_hits(original_text)
    if getting_hit_check then
        settings.getting_beat = true
        settings.last_getting_beat_check = now
        log_message("getting beat up", "debug")
    end
    -- reset it periodically
    if now - settings.last_getting_beat_check > settings.getting_beat_check_delay then
        settings.getting_beat = false
        settings.last_getting_beat_check = now
        log_message("resetting getting_beat state", "debug")
    end
end

function execute_smacking_mob_check(now, original_text)
    settings.smacking_mob_check = check_my_hits(original_text)
    if settings.smacking_mob_check then
        settings.smacking_mob = true
        settings.last_smacking_mob_check = now
        log_message("smacking mob", "debug")
    end
    -- reset it periodically
    if now - settings.last_smacking_mob_check > settings.smacking_mob_check_delay then
        settings.smacking_mob = false
        settings.last_smacking_mob_check = now
        log_message("resetting smacking_mob state", "debug")
    end
    settings.smacking_mob = true
end

function check_party_hits(original)
    local result = string.match(original, "hits " .. settings.player_name .. " for") or
    string.match(original, "hit " .. settings.player_name .. " for") or
    string.match(original, "misses " .. settings.player_name) or
    string.match(original, "miss " .. settings.player_name)
    if result then
        return true
    else
        return false
    end
end

function check_my_hits(original)
    local result = string.match(original, settings.player_name .. " scores a critical hit") or
    string.match(original, settings.player_name .. " hits the")
    if result then
        return true
    else
        return false
    end
end

function run_check(compare)
    if compare == nil or settings.puller then
        do return end
    end

    local now = os.time()
    if now - settings.last_run_trigger < settings.run_trigger_delay then
        do return end
    end
    settings.last_run_trigger = now

    if  string.match(compare, settings.player_name .. " hits the") or
        string.match(compare, settings.player_name .. " scores a critical")
    then
        log_message("close enough to poke, not running", "debug")
        windower.ffxi.run(false)
        do return end
    end

    if  string.match(compare, "You cannot see") or
        string.match(compare, "Unable to see") or
        string.match(compare, "out of range") or
        string.match(compare, "too far away") or
        (current_target and math.sqrt(current_target.distance) > 5)
    then
        -- settings.run_check_count = settings.run_check_count + 1
        -- if settings.run_check_count > settings.max_run_check_count then
        --     log("too many run checks, resetting target")
        --     reset_conditions()
        --     do return end
        -- end
        windower.ffxi.run()
        log_message("running, ugh", "debug")
    end
end

function reset_conditions()
    windower.send_command('input /attack off')
    for k,v in pairs(conditions) do
        conditions[k] = false
    end
    current_target = nil
    log_message('no longer fighting')
    windower.ffxi.run(false)
    -- disengage
    reset_timers()
    settings.smacking_mob = true
end

function reset_timers()
    settings.last_abil_trigger = settings.start_time
    settings.last_engage_trigger = settings.start_time
    settings.last_mob_check_trigger = settings.start_time
    settings.last_ws_trigger = settings.start_time
end

-- this will trigger when VW starts, and when player first engages
-- this means the loop may trigger twice
function check_weaponskill()
    if settings.skip_ws then
        do return end
    end
    if conditions["fighting"] then
        if windower.ffxi.get_player().vitals.tp >= tp_threshold then
            log_message('executing weapon skill')
            execute_weaponskill()
        end
        settings.last_ws_trigger = os.time()
    end
end

function check_for_target()
    if settings.killer then
        log('settings.killer, getting mob by bt')
        current_target = windower.ffxi.get_mob_by_target("bt")
        if current_target and current_target.hpp > 0 then
            windower.send_command('input //settarget ' .. current_target.id)
            windower.send_command('input /attack <bt>')
            windower.ffxi.run()
            coroutine.sleep(0.5)
            windower.ffxi.run(false)
        end
        do return end
    end
    -- distance units are squared, possibly X * Z, so sqrt the output to get distance plugin values
    if settings.puller then
        settings.check_distance = 20.5
    end
    local closest_target = nil
    local target = nil
    local closest_target_distance = 1000
    for _, current_id in pairs(camp_list) do
        target = windower.ffxi.get_mob_by_id(current_id)
        if closest_target == nil and target and target.hpp > 0 then
            log_message('did not have closest_target set, so setting closest to current ' .. current_id, 'debug')
            closest_target = target
        end

        if closest_target then
            closest_target_distance = math.sqrt(closest_target.distance)
            if closest_target_distance < 5 then -- break if current is close enough
                break
            end
        end

        if closest_target and target ~= nil and target.claim_id == 0 and target.hpp > 0 then
            log_message('got a target ' .. current_id .. ', distance is ' .. math.sqrt(target.distance), 'debug')
            local next_target_distance = math.sqrt(target.distance)
            if closest_target_distance == nil then closest_target_distance = 1000 end
            if (
                next_target_distance < settings.check_distance and
                next_target_distance < closest_target_distance
            ) then
                log_message('found a closer target; ' .. current_id .. ' setting closest target', 'debug')
                closest_target = target
            else
                log_message('not closer, checking for next', 'debug')
            end
        end
    end
    if closest_target and closest_target.hpp == 0 then
        return nil
    elseif closest_target ~= nil then
        local message = 'found a ' .. closest_target.name .. ' ' .. closest_target.id .. ' with claim id ' .. closest_target.claim_id .. ' and hp ' .. closest_target.hpp .. ' ' .. math.sqrt(closest_target.distance) .. ' away'
        log_message(message, 'debug')
        return closest_target
    end
end

function engage()
    log_message('engaging ' .. current_target.id .. ', which is ' .. math.sqrt(current_target.distance) .. ' away', 'debug')
    windower.send_command('input //settarget ' .. current_target.id)
    if not settings.running then
        windower.send_command('input /ja ' .. pull_ability .. ' <t>')
    end
    windower.send_command('input /attack <t>')
end

function execute_weaponskill()
    log('executing weapon skill')
    if current_target ~= nil and current_target.hpp > settings.mob_hp_ws_threshold then
        windower.send_command('input /ws "' .. weapon_skill .. '" <t>') -- .. current_target.name .. '"')
    end
end

function use_items()
    log('using items')
    local inventory = windower.ffxi.get_items(0)
    -- TODO: detect whether item is in inventory
    if (windower.ffxi.get_player().vitals.tp) < 1000 and conditions["fighting"] then
        item_command = 'input /item "Dusty Wing" <me>'
        windower.send_command(item_command)
    end
    for k,v in pairs(data.temp_items) do
        item_command = 'input /item "' .. v .. '" <me>'
        windower.send_command(item_command)
    end
end

function execute_abilities()
    -- TODO: check if same effect already active, and skip if so
    log('executing abilities')
    if not abilities then
        do return end
    end
    for k,v in pairs(abilities) do
        has_buff(1)
        if k == 'Bead Pouch' then
            windower.send_command('input /item "' .. k .. '" <' .. v .. '>')
        else
            windower.send_command('input /ja "' .. k .. '" <' .. v .. '>')
        end
        coroutine.sleep(1)
        log('using ' .. k)
    end
end

function has_buff(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    for k,v in pairs(player_buffs) do
        log (k .. ': ' .. v)
        if v == tonumber(target_buff) then
            buff_active = true
            break
        end
    end
    return buff_active
end
