_addon.name = 'My Reisenjima Porter'
_addon.author = 'someguy'
_addon.version = '1.0'
_addon.commands = {'port'}

require('logger')
extdata = require('extdata')
resources = require('resources')

lang = string.lower(windower.ffxi.get_info().language)
local reis_info = {
    [1]={id=26177,japanese='idk',english='"Dim. Ring (Dem)"'  , slot=13},
    -- La Theine zone takes longer to load into, so Dem first
    [2]={id=26176,japanese='idk',english='"Dim. Ring (Holla)"', slot=13},
    [3]={id=26178,japanese='idk',english='"Dim. Ring (Mea)"'  , slot=13},
}
local tav_info = {
    [1]={id=14672,japanese='idk',english='"Tavnazian Ring"', slot=13},
}
local warp_info = {
    [1]={id=28540,japanese='idk',english='"Warp Ring"', slot=13},
}

local item_info = {}

function search_item(cmd)
    local item_array = {}
    local bags = {0,8,10,11,12} --inventory,wardrobe1-4
    local get_items = windower.ffxi.get_items
    for i=1,#bags do
        for _,item in ipairs(get_items(bags[i])) do
            -- -- to determine item id
            -- if item.id ~= 0 then
            --     local item_name = resources.items[tonumber(item.id)].name
            --     log(item_name .. ' ' .. item.id)
            -- end
            if item.id > 0 then
                item_array[item.id] = item
                item_array[item.id].bag = bags[i]
            end
        end
    end
    for index, stats in pairs(item_info) do
        local item = item_array[stats.id]
        local set_equip = windower.ffxi.set_equip
        if item then
            local ext = extdata.decode(item)
            local enchant = ext.type == 'Enchanted Equipment'
            local recast = enchant and ext.charges_remaining > 0 and math.max(ext.next_use_time + 18000 - os.time(), 0)
            local usable = recast and recast == 0
            log(stats[lang], usable and '' or recast and recast .. ' sec recast.')
            if usable or ext.type == 'General' then
                if enchant and item.status ~= 5 then --not equipped
                    set_equip(item.slot, stats.slot, item.bag)
                    log_flag = true
                    repeat --waiting cast delay
                        coroutine.sleep(1)
                        local ext = extdata.decode(get_items(item.bag, item.slot))
                        local delay = ext.activation_time+18000-os.time()
                        if delay > 0 then
                            log(stats[lang], delay)
                        elseif log_flag then
                            log_flag = false
                            log('Item use within 3 seconds...')
                        end
                    until ext.usable or delay > 30
                end
                windower.chat.input('/item ' .. windower.to_shift_jis(stats[lang]) .. ' <me>')
                break;
            end
        else
            log('You don\'t have '..stats[lang]..'.')
        end
    end
end

windower.register_event('addon command', function(command1, ...)
    local args = T{...}
    if command1 == 'tav' then
        item_info = tav_info
    elseif command1 == 'warp' then
        item_info = warp_info
    else
        item_info = reis_info
    end
    search_item()
end)
-- search_item()
