_addon.name = 'Fillmode Toggler'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'fm'}

local fillmode = 0

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    if command1 == 'toggle' then
        if fillmode == 1 then
            fillmode = 0
        else
            fillmode = 1
        end
        windower.send_command('input //fillmode ' .. fillmode)
    end
end)
