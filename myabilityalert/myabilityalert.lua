res = require('resources')
require('logger')

_addon.name = 'MyAbilityAlert'
_addon.author = 'Guy'
_addon.version = '0.0.0'
-- test using aa here
_addon.commands = {'abilityalert'}

whatever_fatal_allure_is = 1234
whatever_dark_thorn_is = 1234

monster_abilities = res.monster_abilities

function find_spell_by_id(spell_id)
    for i,v in pairs(monster_abilities) do
        if (v.id == spell_id) then
            return v.english
        end
    end
end-

function get_action_id(targets)
    for i,v in pairs(targets) do
        for i2,v2 in pairs(v['actions']) do
            if v2['param'] then
                return v2['param']
            end
        end
    end
end

windower.register_event('action', function(action)
    -- Category 7 is the readies message for abilities.
    if (action['category'] == 7) then
        local action_id = get_action_id(action['targets'])
        local spell_id = monster_abilities[action_id]
        log(find_spell_by_id(spell_id))
        log(spell_id)
        if spell_id == whatever_fatal_allure_is then
            log(122, "FATAL ALLURE USED!")
            windower.play_sound(windower.addon_path .. 'sounds/FatalAllureUsed.wav')
        elseif spell_id == whatever_dark_thorn_is then
            log(122, "DARK THORN USED!")
            windower.play_sound(windower.addon_path .. 'sounds/DarkThornUsed.wav')
        end
    end
end)
