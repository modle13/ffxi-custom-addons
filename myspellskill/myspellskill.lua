_addon.name = 'Spellskill'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'spsk'}

res = require('resources')
require('logger')

local chat = windower.add_to_chat

local delay = 7
local running = true
local ignore_combat = false
local skip_count = 0
local max_skip_count = 10000
local food = true
local refresh = true
local haste = true
local march = true
local madrigal = true
local target_food = 'B.E.W. Pitaru'
local convert = false
local skip_convert = false
local keep_invis = false

local player = windower.ffxi.get_player()

local spell_sets = T{
    ["blm"] = {["Aspir"] = "t", ["Drain"] = "t", ["Bio"] = "t", ["Stun"] = "t", ["Bio II"] = "t"},
    ["blu"] = {["Foot Kick"] = "t", ["Power Attack"] = "t"},
    ["blu2"] = {["Foot Kick"] = "t", ["Blood Drain"] = "t"},
    ["blu3"] = {["Foot Kick"] = "t"},
    ["blugimp"] = {["Cocoon"] = "me"},
    ["brd"] = {
        ["Light Threnody"] = "t",
        ["Ice Threnody"] = "t",
        ["Wind Threnody"] = "t",
        ["Earth Threnody"] = "t",
        ["Water Threnody"] = "t",
        ["Ltng. Threnody"] = "t",
        ["Fire Threnody"] = "t",
        ["Dark Threnody"] = "t",
        -- ["Carnage Elegy"] = "t",
        ["Foe Requiem"] = "t",
        ["Foe Requiem II"] = "t",
        -- ["Foe Lullaby"] = "t",
        -- ["Foe Lullaby II"] = "t",
    },
    ["brd_nofight"] = {
        ["Valor Minuet"] = "me",
        ["Army's Paeon"] = "me",
        ["Army's Paeon II"] = "me",
    },
    ["cor"] = {},
    ["drk"] = {},
    ["dark"] = {["Bio"] = "t"},
    ["dark_enfeeb"] = {["Bio"] = "t", ["Aspir"] = "t", ["Dia"] = "t"},
    ["dark_enfeeb_elem"] = {["Bio"] = "t", ["Dia"] = "t", ["Stone"] = "t",},
    ["dark_elem"] = {["Bio"] = "t", ["Stone"] = "t",},
    ["divine"] = {["Flash"] = "t", ["Banish"] = "t", ["Banish II"] = "t", ["Holy"] = "t"},
    ["dia"] = {["Dia"] = "t"},
    ["diaga"] = {["Diaga"] = "t"},
    ["elem"] = {["Stone"] = "t"},
    ["enfeeble"] = {["Dia"] = "t"},
    ["enhance"] = {["Barfira"] = "me", ["Barblizzara"] = "me", ["Barthundra"] = "me", ["Baraera"] = "me"},
    ["enhance1"] = {["Barfire"] = "me", ["Barblizzard"] = "me", ["Barthunder"] = "me", ["Baraero"] = "me"},
    ["geo_dark"] = {["Bio"] = "t", ["Aspir"] = "t"},
    ["geo"] = {["Indi-Poison"] = "me", ["Indi-Voidance"] = "me", ["Geo-Refresh"] = "me"},
    ["geo2"] = {["Indi-Poison"] = "me", ["Indi-Voidance"] = "me", ["bio"] = "t", ["dia"] = "t"},    ["geo4"] = {["Indi-Poison"] = "me",},
    ["geo_fresh"] = {["Geo-Refresh"] = "me"},
    ["healing"] = {["Cure"] = "me", ["Cure"] = "me"},
    ["invis"] = {["Invisible"] = "me",},
    ["nin"] = {["Doton: Ni"] = "t", ["Suiton: Ni"] = "t", ["Hyoton: Ni"] = "t", ["Raiton: Ni"] = "t", ["Huton: Ni"] = "t", ["Katon: Ni"] = "t"},
    ["nin2"] = {["Raiton: Ni"] = "t", ["Huton: Ni"] = "t", ["Katon: Ni"] = "t"},
    ["nin3"] = {["Raiton: Ni"] = "t", ["Hyoton: Ni"] = "t", ["Katon: Ni"] = "t", ["Doton: Ni"] = "t"},
    ["pld"] = {["Cocoon"] = "me", ["Majesty"] = "me"},
    ["pollen"] = {["Pollen"] = "me"},
    ["protect"] = {["Protect"] "t"},
    ["provoke"] = {["Provoke"] = "t"},
    ["rng"] = {["Haste Samba"] = "me", ["Stutter Step"] = "t", ["Violent Flourish"] = "t"},
    ["ra"] = {["ra"] = "t",},
    ["rdm"] = {["Barfire"] = "me",["Barwater"] = "me",["Barstone"] = "me",["Baraero"] = "me"},
    ["sam"] = {["Hasso"] = "me", ["Haste Samba"] = "me",},
    ["skip"] = {["Skip"] = "me",},
    ["aggro"] = {["Diaga"] = "nearest", ["Protect"] = "p4",},
    ["smn"] = {["Air Spirit"] = "me",["Release"] = "me"},
    ["smn2"] = {["Water Spirit"] = "me",["Release"] = "me"},
    ["thf"] = {["Provoke"] = "t"},
    ["voke"] = {["Provoke"] = "t"},
    ["whm_mix"] = {["Cure"] = "me", ["Flash"] = "t", ["Barwater"] = "me", ["Baraero"] = "me", ["Barfira"] = "me", ["Banish"] = "t", ["Dia"] = "t", ["Cure"] = "me", ["Cure"] = "me"},
    ["whm_enf_heal_divine"] = {["Cure"] = "me", ["Flash"] = "t", ["Banish"] = "t", ["Dia"] = "t", ["Cure"] = "me", ["Cure"] = "me"},
    ["whm_enf_divine"] = {["Flash"] = "t", ["Banish II"] = "t", ["Banish"] = "t", ["Dia"] = "t", ["Dia II"] = "t"},
    ["whm_enh_enf_heal"] = {["Cure"] = "me", ["Barwater"] = "me", ["Baraero"] = "me", ["Barfire"] = "me", ["Dia"] = "t", ["Cure"] = "me", ["Dia"] = "t"},
    ["whm_enh_heal"] = {["Cure"] = "me", ["Barwater"] = "me", ["Baraero"] = "me", ["Barfire"] = "me", ["Cure"] = "me"},
    ["whm_heal"] = {["Cure"] = "me", ["Cure"] = "me"},
    ["whm"] = {["Baraera"] = "me", ["Barfira"] = "me", ["Cure"] = "me", ["Cure"] = "me", ["Cure"] = "me", ["Cure"] = "me", ["Cure"] = "me"},
    ["whm_group"] = {["Baraera"] = "me", ["Barfira"] = "me", ["Cure"] = "p1", ["Cure II"] = "p1", ["Cure III"] = "p1", ["Cura"] = "me", ["Curaga"] = "p1"},
}

local job_buffs_map_main = {
    ["SAM"] = {["Meditate"] = "me", ["Hasso"] = "me",},
    ["WHM"] = {["Haste"] = "me", ["Afflatus Solace"] = "me",},
    ["RDM"] = {["Haste II"] = "me", ["Refresh II"] = "me", ["Composure"] = "me",},
}

local job_buffs_map_sub = {
    ["SAM"] = {["Meditate"] = "me", ["Hasso"] = "me",},
    ["WHM"] = {["Haste"] = "me",},
    ["RDM"] = {["Haste"] = "me", ["Refresh"] = "me",},
}

local buff_id_map = {
    ["Haste"] = 33,
    ["Refresh"] = 43,
}

-- pink = 61
-- blue = 30
-- green = 50
-- orage = 25
-- dark red = 28
-- light red = 123
-- dark yellow = 36
-- light yellow = 37
-- light pink = 8
-- dim yellow = 53
-- purple = 271

-- set default to current class abbreviation if it exists

local player_name = windower.ffxi.get_player().name
local player_main_job = windower.ffxi.get_player().main_job
local player_sub_job = windower.ffxi.get_player().sub_job
current_set_name = player_main_job:lower()

chat(30, _addon.name .. ': choosing set based on player job: ' .. player_main_job)
selected_set = spell_sets[current_set_name]

last_cast = os.time()

function run_it()
    running = true
    skip_count = 0
    chat(30, _addon.name .. ' RUNNING')

    while running do
        magics()
        -- this sleep is here so we don't overflow the client when magics returns immediately
        coroutine.sleep(1)
    end
end

function stop_it()
    running = false
    chat(125, _addon.name .. ' STOPPING')
end

function have_target()
    local target = windower.ffxi.get_mob_by_target("t")
    return target ~= nil
end

function show_buffs()
    log('showing player buffs')
    local all_buffs = res.buffs
    local player_buffs = windower.ffxi.get_player()["buffs"]
    for k,v in pairs(all_buffs) do
        for k2,v2 in pairs(player_buffs) do
            if v.id == v2 then
                local id_as_string = tostring(v.id)
                id_print_string = pad_this_string(id_as_string, 5)
                log(id_print_string .. '    :    ' .. v.en)
            end
        end
    end
end

function pad_this_string(input_string, padding)
    local padded_so_far = 0
    local to_pad = padding - string.len(input_string)
    while padded_so_far < to_pad do
        input_string = " " .. input_string
        padded_so_far = padded_so_far + 1
    end
    return input_string
end

function has_buff_by_name(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    local target_buff_id = res.buffs:with('name', target_buff).id

    log('checking for ' .. target_buff .. ' with target id ' .. target_buff_id)

    for k,v in pairs(player_buffs) do
        if v == tonumber(target_buff_id) then
            buff_active = true
            log('buff already active : ' .. target_buff)
            break
        end
    end
    if not buff_active then
        log('BUFF NOT ACTIVE : ' .. target_buff)
    end
    return buff_active
end

function magics()
    local cast = true
    for k,v in pairs(selected_set) do
        if not running or (not have_target() and not ignore_combat) or skip_count > max_skip_count then
            stop_it()
            break
        end
        if v == "t" and not have_target() then
            cast = false
        else
            cast = true
        end
        log(k)

        local spell_command = 'input /ma "' .. k .. '" <' .. v .. '>'
        if k == 'ra' then
            spell_command = 'input /ra <t>'
        end

        if v == 'nearest' then
            windower.send_command('setkey f8 down;wait 0.2;setkey f8 up;')
            coroutine.sleep(1)
            spell_command = 'input /ma "' .. k .. '" <t>'
        end

        if k == "Skip" then
            spell_command = "skip"
        end

        if      player.main_job:lower() ~= 'brd' and
                refresh and not
                has_buff_by_name('Refresh')
        then
            if player.sub_job:lower() == 'rdm' then
                spell_command = 'input /ma Refresh <me>'
                log('Refresh')
            elseif player.main_job:lower() == 'rdm' then
                spell_command = 'input /ma "Refresh II" <me>'
                log('Refresh II')
            end
        elseif  player.main_job:lower() == 'brd' and
                march and not
                has_buff_by_name('March')
        then
            local target_spell = 'Victory March'
            -- different tiers of spell have the same id, so might have to do a count
            spell_command = 'input /ma "' .. target_spell .. '" <me>'
            log(target_spell)
        elseif  player.main_job:lower() == 'brd' and
                madrigal and not
                has_buff_by_name('Madrigal')
        then
            local target_spell = 'Sword Madrigal'
            spell_command = 'input /ma "' .. target_spell .. '" <me>'
            log(target_spell)
        elseif  (player.main_job:lower() == 'whm' or player.sub_job:lower() == 'whm' or player.sub_job:lower() == 'rdm') and
                haste and
                not has_buff_by_name('Haste')
        then
            spell_command = 'input /ma Haste <me>'
            log('Haste')
        elseif not has_buff_by_name('Food') and target_food ~= '' and food then
            spell_command = 'input /item "' .. target_food ..'" <me>'
            log('Fooding')
        elseif convert and not skip_convert then
            spell_command = 'input /ja "Convert" <me>'
            log('converting')
        end
        if cast and spell_command ~= 'skip' then
            if k == 'Invisible' then
                keep_invis = true
                if not has_buff_by_name('Invisible') then
                    windower.send_command(spell_command)
                    last_cast = os.time()
                end
            else
                keep_invis = false
                windower.send_command(spell_command)
                last_cast = os.time()
            end
            coroutine.sleep(2)
        else
            chat(122, 'Skipping cast of ' .. k)
            last_cast = os.time()
        end
        while os.time() - last_cast < delay do
            coroutine.sleep(1)
        end
    end
end

function recast_now()
    last_cast = os.time() - delay * 2
end

function skip_cast()
    skip_count = skip_count + 1
    if not windower.ffxi.get_player().in_combat and not ignore_combat then
        stop_it()
    end
    chat(125, "skip count is now " .. skip_count)
end

function get_spell_sets()
    local keyset={}
    local n=0

    for k,v in pairs(spell_sets) do
        n = n + 1
        keyset[n] = k
    end
    log("available spell skill sets are:")
    log(table.concat(keyset, ", "))
end

function show_set(target_set)
    local keyset={}
    local n=0

    for k,v in pairs(spell_sets[target_set]) do
        n = n + 1
        keyset[n] = k
    end
    chat(122, 'spells in set ' .. target_set .. " are:")
    log(table.concat(keyset, ", "))
end

function change_set(target_set)
    selected_set = spell_sets[target_set]
    chat(122, 'spell set is now: ' .. target_set)
    show_set(target_set)
end

function set_delay(value_to_set)
    delay = tonumber(value_to_set)
    chat(122, 'Delay is set to: ' .. tostring(delay) .. '.')
end

function set_ignore_combat(value_to_set)
    ignore_combat = value_to_set
    chat(122, 'Ignore combat is set to: ' .. tostring(ignore_combat) .. '.')
end

function show_target_set(set_to_show)
    if set_to_show == nil or set_to_show == '' then
        set_to_show = current_set_name
    end
    chat(122, 'Set to show is: ' .. set_to_show)
    show_set(set_to_show)
end

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    -- battlemod breaks this
    if string.match(original, player_name .. " casts") then
        log("saw a completed cast")
        recast_now()
    -- battlemod breaks this
    elseif string.match(original, player_name .. " starts casting") then
        log("saw a started cast")
        if skip_count > 0 then
            chat(8, 'skip count reset')
            skip_count = 0
        end
        last_cast = os.time()
    elseif string.match(original, player_name .. "'s casting is interrupted") then
        log("saw an interrupted cast")
        recast_now()
    elseif string.match(original, "has already placed a") then
        coroutine.sleep(delay)
        skip_cast()
    elseif string.match(original, "Skipping cast of") then
        skip_cast()
    elseif string.match(original, "does not have enough MP") then
        log("out of mp")
        if player_main_job:lower() == 'rdm' or player_sub_job:lower() == 'rdm' then
            convert = true
            log("can convert")
        end
        skip_cast()
    -- battlemod breaks this
    elseif string.match(original, "uses Convert.") then
        convert = false
    elseif string.match(original, "Unable to cast") then
        skip_cast()
    elseif string.match(original, "You cannot perform that action on the selected") then
        skip_cast()
    elseif string.match(original, "The effect of Invisible is about to wear off.") then
        if keep_invis then
            local spell_command = 'input /ma "Invisible" <me>'
            windower.send_command(spell_command)
        end
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = table.concat({...}, ' ')
    if command1 == 'run' or command1 == 'start' then
        run_it()
    elseif command1 == 'stop' then
        stop_it()
    elseif command1 == 'sets' then
        get_spell_sets()
    elseif command1 == 'plain' then
        food = false
        refresh = false
    elseif command1 == 'invis' then
        skip_convert = true
        haste = false
        madrigal = false
        march = false
        food = false
        refresh = false
    elseif command1 == 'food' then
        food = not food
    elseif command1 == 'refresh' then
        refresh = not refresh
        log('setting refresh: ' .. tostring(refresh))
    elseif command1 == 'convert' then
        skip_convert = not skip_convert
        log('setting skip_convert: ' .. tostring(skip_convert))
    elseif command1 == 'haste' then
        haste = not haste
        log('setting haste: ' .. tostring(haste))
    elseif command1 == 'march' then
        march = not march
        log('setting march: ' .. tostring(march))
    elseif command1 == 'madrigal' then
        madrigal = not madrigal
        log('setting madrigal: ' .. tostring(madrigal))
    elseif command1 == 'fast' then
        set_delay(1)
        set_ignore_combat(true)
    elseif command1 == 'change' or command1 == 'set' then
        change_set(cmd_remainder)
    elseif command1 == 'show' then
        show_target_set(cmd_remainder)
    elseif command1 == 'showbuffs' then
        show_buffs()
    elseif command1 == 'combat' then
        local new_val = not ignore_combat
        set_ignore_combat(new_val)
    elseif command1 == 'delay' then
        set_delay(cmd_remainder)
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)

show_set(current_set_name)
