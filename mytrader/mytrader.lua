_addon.name = 'Trader'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'trade'}

require('logger')

start_time = os.time()

local delay = 7
local trade_item = "SP Gobbie Key"
-- local trade_item = "Dial Key #Ab"
local target = nil

function run_it()
    running = true
    log("running")
    while running do
        if have_target() then
            coroutine.sleep(1)
            do_trade()
        else
            log("no target")
            stop_it()
        end
        coroutine.sleep(delay)
    end
end

function have_target()
    if target == nil then
        target = windower.ffxi.get_mob_by_target("t")
    end
    return target ~= nil
end

function do_trade()
    log("targeting " .. target.name)
    target_command = 'input //settarget ' .. target.id
    windower.send_command(target_command)
    coroutine.sleep(1)
    log("trading with " .. target.name)
    item_command = 'input /item "' .. trade_item .. '" <t>'
    windower.send_command(item_command)
end

function stop_it()
    running = false
    log("stopping")
end

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    if string.match(original, "A command error occurred") then
        do_trade()
        -- stop_it()
    end
end)

windower.register_event('addon command', function(command1, command2, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = {...}
    if command1 == 'run' or command1 == 'start' then
        run_it()
    elseif command1 == 'stop' then
        stop_it()
    elseif command1 == 'item' then
        trade_item = command2
        log('trade item set to: ' .. tostring(trade_item))
    elseif command1 == 'delay' then
        if tonumber(command2) ~= nil then
            delay = command2
            log('delay set to: ' .. tostring(delay))
        else
            log('invalid value for delay: ' .. command2)
        end
    elseif command1 == 'item' then
        trade_item = table.concat(cmd_remainder, ' ')
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)
