_addon.name = 'My Buffs'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'buffs'}

-- luau contains the config loader
require('luau')
require('logger')
local chat = windower.add_to_chat
-- chat(207, "chat reference")

local player_name = windower.ffxi.get_player().name

-- create a set with input string
-- //buff create myset
-- take an input string as a buff name
-- add it to a list
-- //buff add mybuff

settings = config.load(defaults)


windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    -- battlemod breaks this
    if string.match(original, player_name .. " casts") then
        log("saw a completed cast")
    end
end)

windower.register_event('addon command', function(command, ...)
    command = command and command:lower()
    if command == 'help' then
        print('%s v%s':format(_addon.name, _addon.version))
        print('    \\cs(255,255,255)no help for you\\cr - yo')
    elseif command == 'test' then
        test()
    elseif command == 'show' then
        showbuffs()
    end
end)


function showbuffs()
    local player = windower.ffxi.get_player()
    buffs = player["buffs"]
    for k,v in pairs(buffs) do
        log (k .. ': ' .. v)
    end
end

function test()
    log('testing')
    chat(201, 'testing')
    chat(201, settings.mode)
    chat(201, settings.rdm.spells)
end

function has_buff_by_name(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    local target_buff_id = res.buffs:with('name', target_buff).id

    for k,v in pairs(player_buffs) do
        if v == tonumber(target_buff_id) then
            buff_active = true
            break
        end
    end
    return buff_active
end

test()

