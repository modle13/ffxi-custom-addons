include('WAR_include.lua')

JA = {
    ['Ctrl+1'] = 'input /ja "Sneak Attack"',
    ['Ctrl+2'] = 'input /ja "Trick Attack"',
    ['Ctrl+3'] = 'input /ja "Assassin\'s Charge"',
    ['Alt+2'] = WAR,                -- Goes to WAR sub table
}

WS = {
    ['Ctrl+3'] = 'input /ja "Angon" <t>',
    ['Alt+2'] = 'input /ws "Rudra\'s Storm" <t>',
    ['Alt+3'] = 'input /ws "Savage Blade" <t>',
}

return {
    ['Ctrl+1'] = 'input /echo asdf',
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
