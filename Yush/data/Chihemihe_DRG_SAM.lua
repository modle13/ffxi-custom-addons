include('SAM_include.lua')

JA = {
    ['Ctrl+1'] = 'input //ju normal',
    ['Ctrl+2'] = 'input //ju threat',
    ['Ctrl+3'] = 'input /ja "Restoring Breath"',
    ['Ctrl+4'] = 'input /equip ammo Angon; input /ja "Angon" <t>',
    ['Alt+2'] = SAM,                -- Goes to SAM sub table
}

WS = {
    ['Alt+2'] = 'input /ws "Impulse Drive" <t>',
}

return {
    ['Ctrl+1'] = 'input /echo asdf',
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
