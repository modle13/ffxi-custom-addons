include('SAM_include.lua')

JA = {
    ['Ctrl+1'] = 'input /ja Konzen-ittai',
    ['Ctrl+2'] = 'input /ja provoke',
    ['Ctrl+3'] = 'input /ja "Restoring Breath"',
    ['Ctrl+4'] = 'input /ja berserk; wait 1; input /ja warcry',
    ['Alt+2'] = SAM,                -- Goes to SAM sub table
}

WS = {
    ['Alt+2'] = 'input /ws "Tachi: Gekko" <t>',
}

return {
    ['Ctrl+1'] = 'input /echo asdf',
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
