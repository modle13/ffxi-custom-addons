include('WAR_include.lua')

JA = {
    ['Ctrl+1'] = 'input /ja "Sneak Attack"',
    ['Ctrl+2'] = 'input /ja "Trick Attack"',
    ['Ctrl+3'] = 'input /ja "Warrior\'s Charge"',
    ['Ctrl+4'] = 'input /ja "Retaliation"',
    ['Ctrl+5'] = 'input /ja "Blood Rage"',
    ['Ctrl+6'] = 'input /ja "Restraint"',
    ['Alt+2'] = WAR,                -- Goes to WAR sub table
}

WS = {
    ['Alt+2'] = 'input /ws "Vorpal Blade" <t>',
}

return {
    ['Ctrl+1'] = 'input /echo asdf',
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
