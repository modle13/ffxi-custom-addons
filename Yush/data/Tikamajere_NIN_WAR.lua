include('WAR_include.lua')

JA = {
    ['Ctrl+1'] = 'input /ja "Yonin" <me>',
    ['Ctrl+4'] = 'input /ja "Innin" <me>',
    ['Alt+2'] = WAR,                -- Goes to WAR sub table
}

WS = {
    -- ['Ctrl+3'] = 'input /ws "Blade: Jin" <t>',
    ['Ctrl+3'] = 'input /ws "Tachi: Kasha" <t>',
}

return {
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
