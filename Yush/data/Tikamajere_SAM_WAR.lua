include('WAR_include.lua')

JA = {
    ['Ctrl+1'] = 'input /ja "Third Eye" <me>',
    ['Ctrl+4'] = 'input /ja "Hasso" <me>',
    ['Alt+2'] = WAR,                -- Goes to WAR sub table
}

WS = {
    ['Ctrl+3'] = 'input /ws "Tachi: Enpi" <t>',
}

return {
    ['Alt+2'] = JA,                 -- Goes to JA sub table
    ['Alt+5'] = WS,                 -- Goes to WS sub table
}
