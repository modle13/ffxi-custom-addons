_addon.name = 'My Spell Tracker'
_addon.author = 'Guy'
_addon.version = '0.1.0'
_addon.commands = {'ambu'}

res = require('resources')
file = require('files')
require('logger')

local chat = windower.add_to_chat
local storages_path = 'data/'
local abilities = T{}
local food = ""
local player_name = ""
local ambu_set = "ambu"
local settings = T{}

-- set lockstyle
-- eat food
-- cast necessary buffs/spells, ja works for spells too


function dump_to_file()
    settings["food"] = food
    settings["abilities"] = abilities

    log(settings)
    local player_name = windower.ffxi.get_player().name
    local self_storage = file.new(storages_path .. '\\' .. player_name .. '.lua')

    if not self_storage:exists() then
        self_storage:create()
    end

    self_storage:write('return ' .. make_table(abilities, 0) .. '\n')
    collectgarbage()
end

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = {...}
    if command1 == 'run' or command1 == 'start' then
        run_it()
    elseif command1 == 'abil' or command1 == 'ability' or command1 == 'spell' then
        tpp_input = table.concat(cmd_remainder, ' ')
        -- add spell to table
        table.insert(abilities, tpp_input)
        chat(122,'Added ability/spell: ' .. tpp_input .. '.')
    elseif command1 == 'food' then
        tpp_input = table.concat(cmd_remainder, ' ')
        -- add spell to table
        food = tpp_input
        chat(122,'food is: ' .. tpp_input .. '.')
    elseif command1 == 'test' then
        dump_to_file()
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)
