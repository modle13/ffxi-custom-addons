_addon.name = 'MyCapacityWatch'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'mycp'}

require('logger')

-- to reset any endless recursions :/
reload_time = 300
start_time = os.time()

local target_lists = {
    ["reisF11"] = {17969187,17969188,17969189,17969190,17969191,17969192,17969193,17969206,17969207,17969208,17969209,},
    ["reisF11_2"] = {17969197,17969201,17969202,17969203,17969204,17969207},
    ["test"] = {17195406,17195391,},
}

local running = false
local current_target = nil

local camp_list = target_lists['reisF11']
local camp_list = target_lists['test']
local camp_list = target_lists['reisF11_2']

local ws_sleep_time = 3

local ws_delay = 3
local engage_delay = 2
local ability_delay = 45
local mob_check_delay = 1

local last_ws_trigger = 0
local last_engage_trigger = 0
local last_abil_trigger = 0
local last_mob_check_trigger = 0

local weapon_skill = "Evisceration"
local tp_threshold = 1000
local job = windower.ffxi.get_player().main_job
local player_name = windower.ffxi.get_player().name
local temp_items = {"Dusty Wing", "Stalwart's Tonic"} --, "Braver's Drink", "Champion's Tonic"}

local abilities = {["Meditate"] = "me", ["Hasso"] = "me", ["Berserk"] = "me", ["Warcry"] = "me",}
local job_abil = {
    ["SAM"] = {["Meditate"] = "me", ["Hasso"] = "me", ["Berserk"] = "me", ["Warcry"] = "me",},
    ["DRG"] = {
        ["Angon"] = "t", ["Meditate"] = "me",
        ["Hasso"] = "me", ["Jump"] = "t",
        ["High Jump"] = "t", ["Soul Jump"] = "t",
        ["Spirit Jump"] = "t",
    },
    ["THF"] = {["Berserk"] = "me", ["Warcry"] = "me"},
    ["BRD"] = {["Valor Minuet IV"] = "me"},
}
local weaponskills = {
    -- ["Chihemihe"] = "Savage Blade",
    ["Chihemihe"] = "Impulse Drive",
    -- ["Chihemihe"] = "Rudra's Storm",
    -- ["Chihemihe"] = "Evisceration",
    -- ["Kitiaramatar"] = "Savage Blade",
    ["Kitiaramatar"] = "Rudra's Storm",
    -- ["Kitiaramatar"] = "Evisceration",
    ["Tikamajere"] = "Tachi: Fudo",
    -- ["Tikamajere"] = "Tachi Shoha",
    ["Crowmane"] = "Tachi: Shoha"
}

local tp_thresholds = {
    ["Savage Blade" ] = 1750,
    ["Impulse Drive"] = 1750,
    ["Evisceration" ] = 1000,
    ["Rudra's Storm"] = 1000,
    ["Tachi: Shoha"  ] = 1000,
    ["Tachi: Fudo"   ] = 1000,
}

local conditions = {
    fighting    = false,
    engaged     = false,
    mob_present = false,
}

-----------------------------
-- main function           --
-----------------------------

function run()
    while running do
        main()
        coroutine.sleep(1)
    end
end

function main()
    local now = os.time()

    if not conditions["mob_present"] and now - last_mob_check_trigger > mob_check_delay then
        last_mob_check_trigger = now
        current_target = check_for_target()
        if current_target ~= nil then
            log("starting")
            conditions["mob_present"] = true
        end
    end

    -- start check
    if conditions["mob_present"] and not conditions["engaged"] and now - last_engage_trigger > engage_delay then
        log('engage check triggered')
        last_engage_trigger = now
        engage()
        log("engaged; job: " .. job .. ", ws: " .. weapon_skill .. ", tp_threshold: " .. tp_threshold)
    end

    if conditions["engaged"] and now - last_ws_trigger > ws_delay then
        last_ws_trigger = now
        check_weaponskill()
    end

    local tp_threshold_met = windower.ffxi.get_player().vitals.tp >= tp_threshold

    if conditions["engaged"] and now - last_abil_trigger > ability_delay and not tp_threshold_met then
        log('ability check triggered')
        last_abil_trigger = now
        execute_abilities()
    end
end


-----------------------------
-- windower event triggers --
-----------------------------

windower.register_event('incoming text', function(original, new, color)
    -- this will trigger infinitely off of its own log messages; be careful
    -- log('triggering incoming text')
    local now = os.time()

    if current_target == nil then
        do return end
    end

    original = original:strip_format()

    if string.match(original, "You can only use that command during battle") then
        reset_conditions()
    end

    if string.match(original, "You cannot use that command while unconscious") then
        reset_conditions()
        running = false
    end

    run_check(original)

    -- end check
    if current_target ~= nil and (
        string.match(original, "defeats " .. current_target.name) or
        string.match(original, "defeats the " .. current_target.name)
    ) then
        reset_conditions()
    end

    -- engagement check
    if current_target ~= nil and (
        string.match(original, "hits " .. current_target.name .. " for") or
        string.match(original, "hits the " .. current_target.name .. " for")
    ) then
        conditions["engaged"] = true
        conditions["fighting"] = true
        windower.ffxi.run(false)
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    if command1 == 'start' or command1 == 'run' then
        running = true
        log('running operation')
        run()
    elseif command1 == 'stop' then
        running = false
        log('stopping operation')
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)


-----------------------------
--        functions        --
-----------------------------

function run_check(compare)
    if string.match(compare, "Unable to see") or string.match(compare, "out of range") or string.match(compare, "too far away") then
        windower.ffxi.run()
        log("running, ugh")
    end
end

function reset_conditions()
    for k,v in pairs(conditions) do
        conditions[k] = false
    end
    current_target = nil
    log('no longer fighting')
    windower.ffxi.run(false)
end

-- this will trigger when VW starts, and when player first engages
-- this means the loop may trigger twice
function check_weaponskill()
    if conditions["fighting"] then
        if windower.ffxi.get_player().vitals.tp >= tp_threshold then
            log('executing weapon skill')
            execute_weaponskill()
        end
        last_ws_trigger = os.time()
    end
end

function check_for_target()
    local target = nil
    for _, current_id in pairs(camp_list) do
        target = windower.ffxi.get_mob_by_id(current_id)
        if target ~= nil and target.hpp > 0 then
            log('found a ' .. target.name)
            break
        end
    end
    log('claim id ' .. tostring(target['claim_id']))
    target = nil
    -- for k,v in pairs(target) do
        -- log('key: ' .. k .. ', value: ' .. tostring(v))
    -- end
    return target
end

function engage()
    log('engaging ' .. current_target.id)
    abilities = job_abil[job]
    weapon_skill = weaponskills[player_name]
    tp_threshold = tp_thresholds[weapon_skill]
    windower.send_command('input //settarget ' .. current_target.id)
    windower.send_command('input /attack <t>')
end

function execute_weaponskill()
    log('executing weapon skill')
    windower.send_command('input /ws "' .. weapon_skill .. '" <t>') -- .. current_target.name .. '"')
end

function use_items()
    log('using items')
    local inventory = windower.ffxi.get_items(0)
    -- TODO: detect whether item is in inventory
    if (windower.ffxi.get_player().vitals.tp) < 1000 and conditions["fighting"] then
        item_command = 'input /item "Dusty Wing" <me>'
        windower.send_command(item_command)
    end
    for k,v in pairs(temp_items) do
        item_command = 'input /item "' .. v .. '" <me>'
        windower.send_command(item_command)
    end
end

function execute_abilities()
    log('executing abilities')
    for k,v in pairs(abilities) do
        windower.send_command('input /ja "' .. k .. '" <' .. v .. '>')
        coroutine.sleep(3)
        log('using ' .. k)
    end
end
