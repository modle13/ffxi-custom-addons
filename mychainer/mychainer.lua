_addon.name = 'MyChainer'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'chain'}

require('logger')

local emotes = {"clap", "cheer", "wave"}
local running = false
local weapon_skill = nil
local watch_skill = nil
local start_time = os.time()
local trigger_delay = 1
local last_trigger = start_time

windower.register_event('incoming text', function(original, new, color)
    local now = os.time()
    if not running then
        do return end
    end
    if watch_skill ~= nil and string.match(original, 'readies ' .. tostring(watch_skill)) then
        if now - last_trigger < trigger_delay then
            do return end
        end
        last_trigger = now
        log('saw matched ws ' .. tostring(watch_skill) .. '; preparing to chain with ' .. tostring(weapon_skill))
        coroutine.sleep(4)
        windower.send_command('input /ws ' .. tostring(weapon_skill))
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = table.concat({...}, ' ')
    if command1 == 'run' or command1 == 'start' then
        running = true
        log("running")
        start_time = os.time()
    elseif command1 == 'stop' then
        running = false
        log("stopping")
    elseif command1 == 'ws' then
        weapon_skill = cmd_remainder
        log("ws is " .. tostring(weapon_skill))
    elseif command1 == 'watch' then
        watch_skill = cmd_remainder
        log("watch is " .. tostring(watch_skill))
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)