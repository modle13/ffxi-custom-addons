_addon.name = 'MyAlerter'
_addon.author = 'Guy'
_addon.version = '0.1.0'
_addon.commands = {'alerter', 'al'}

require('logger')

local message = "no message set"

local chat = windower.add_to_chat

-- check for target monster id spawn and /send all echo

function send_message(message)
    windower.send_command('input //send @all /echo ' .. message)
end

function ring_alarm()
    if message == nil then
        message = 'alarm~ weewooweewoo'
    end
    send_message(message)
    coroutine.sleep(5)
    send_message(message)
    coroutine.sleep(5)
    send_message(message)
    message = nil
end

windower.register_event('addon command', function(command, ...)
    command = command and command:lower() or 'help'
    args = L{...}
    if command == 'run' or command == 'start' then
        run_it()
    elseif command == 'alarm' then
        if args[1] == nil or args[2] == nil then
            log('error: expected `alarm time_in_seconds message`')
            return
        end
        message = args[2]
        local time = args[1]
        log("message is " .. message .. " and time is " .. time)
        coroutine.schedule(ring_alarm, time * 60)
    elseif command == 'stop' then
        stop_it()
    elseif command == 'test' then
        log('testing')
        send_message('the message')
    else
        log('command is bad: "' .. command .. '"')
    end
end)

send_message("loaded")
