_addon.name = 'Commnity Bazaar'
_addon.author = 'sapcehamster'
_addon.commands = {'cz'} -- check for conflicts with other addons
_addon.version = '0.1.0'
_addon.language = 'english'

-- luau contains the config loader
require('luau')
require('logger')

-- features
-- 

settings = config.load(defaults)

log(settings)

log(settings.whitelist)

-- big problems to solve
-- 1. autotrade
-- 2. autobazaar
-- 3. update prices mule has with tells
-- 4. payout with messages (more autotrade)
-- 5. register sales
-- 6. track items with character that traded


------ price setting
-- next level shit which probably isn't possible would be to be able to talk to said bazaar mule to tell him
-- what prices to sell stuff at, so you could control him from any of the whitelisted characters.
-- could even think creatively about how you might get a payout from him via autotrade and /tell commands.
--  /t sushichef sell {Sublime sushi +1} 6 stacks 40k
-- would call autobazaar
-- -- //autobazaar sell {Sublime Sushi +1} 6 stacks 40k


------ payout
-- /t sushichef payout 1m
-- which would just trade you back the amount you specify.


--client
-- idea being you have a community bazaar mule between a small group of people or maybe just for someone's
-- personal army, and you could dump stuff to sell on them via whitelist autotrade,
-- and it'd be setup on the mule to auto bazaar it for some price once it sees it in the inventory

---------- stretch feature
-- next level shit which probably isn't possible would be to be able to talk to said bazaar mule to tell him
-- what prices to sell stuff at, so you could control him from any of the whitelisted characters.
-- could even think creatively about how you might get a payout from him via autotrade and /tell commands.

-- you'd set prices by just /telling the bazaar mule that's running the addon some syntax like 
--  /t sushichef sell {Sublime sushi +1} 6 stacks 40k
-- which would basically be a command that the bazaar addon sees and then spits out something like 
-- //autobazaar sell {Sublime Sushi +1} 6 stacks 40k
-- which would make sure that the bazaar has, at most, 6 stacks worth of sushi for sale set at 40k per sushi, or somerthing like that. that is the same syntax used for auctionhelper. could do a lot more with it too but getting your payout woul work the same way. you'd initiate a trade, it'd auto accept, and you say say like 
-- /t sushichef payout {Sublime Sushi +1}
-- and that would autotrade you gil made from sushi sold that was traded from that character and you just complete the trade. or simply something like
-- /t sushichef payout 1m
-- which would just trade you back the amount you specify.


--me
-- i think all of that is possible
-- there's a bazaar addon already i think (this is probably autobazaar),
-- and the trade (actually this is probably packets) and list commands
-- to trigger that would just be text parsing
-- the mapping and tracking would be a bit more challenging
-- you'd have to store that off to disk


--client
-- even without tracking, something like 
-- /t sushichef payout {Sublime Sushi +1} 12
-- to just give you 12x whatever the currently set price is for that item. so you wouldn't have to have anything complex going on in the background and the payee woudln't have to worry about what prices the items were sold at and have to calculate their payout.
-- anyway, just some thoughts that have come up recently that i'd be more than happy to fund some tinkering cycles on if it was something you were interesting in doing. i'd kill for autopass tbh (edited)


--me
-- yea it has merit
-- i'd be concerned with abuse
-- like, you could just guess items
-- but if you use whitelist and trust everyone else that's fine
-- i'd prefer to remove that element completely and register who traded what


--client
-- if building the tracking wouldn't be a significant amount of work, it would feel pretty magical to use. i think the manual forced payouts by item or amount would be looked at as a backup, and i would think a whitelist, exactly like autoinvite, would be enough to ensure only trusted accounts could initiate those commands. 

-- the use case i shared with a communal bazaar npc account between multiple people is kind of niche, and would only be used like that within a tight trusted friend group. other uses of the same thing with bazaar and payout would just be between my spheres mule in pj and am while i'm working on the focuser, for example. or, i'd simply just use the auto bazaar part to set my own bazaar on an auto-pilot to stay stocked and batch adjust prices via 1 command. (edited)
-- also yeah the shorthand built into the autoinvite command syntax is cool. loving that shortcut addon btw... although it's weird sometimes cause if you accidentally do something like //yell {Sublime Sushi +1}. it'll work as a yell but it wont be auto translated. (edited)
-- September 27, 2021

