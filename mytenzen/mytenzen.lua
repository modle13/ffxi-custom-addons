_addon.name = 'MyTenzen'
_addon.author = 'Guy'
_addon.version = '0.1.0'
_addon.commands = {'tenzen', 'ten'}

require('logger')

local chat = windower.add_to_chat
-- chat(207,"chat reference")

local run = false
local sleep_time = 1
local engaged = false
local hpp_threshold = 20
local sane_threshold = 900
local ignore_conditions = false
local player_name = windower.ffxi.get_player().name
local player_tp = 1000
local trick_attack = false
local berserk = true
local ws_count = 0
local chain = true

-- this format is required for table:containskey(somestring)
local touchy_trusts = T{
    ["Tenzen"] = true
}
local sane_trusts = T{
    ["Shantotto"] = true,
    ["ArkEV"] = true,
    ["Lion"] = true,
    ["Zeid"] = true,
    ["Amchuchu"] = true,
    ["Rongelouts"] = true,
    ["Trion"] = true,
    ["Prishe "] = true,
}

local ws = "Brainshaker"
local ws_list = T{}
table.insert(ws_list, ws)

found_touchy = T{}
found_sane = T{}

log("starting script")

function run_it()
    if ws == nil then
        log("ws is not set")
        return
    end
    running = true
    log("running")
    while running do
        if ignore_conditions and can_ws() then
            execute_ws()
        elseif can_ws() then
            check_for_trusts()
            process_cycle()
        end
        coroutine.sleep(sleep_time)
    end
end

function stop_it()
    running = false
    log("stopping")
end

function can_ws()
    check_combat()
    if not engaged then
        return false
    end
    local tp = windower.ffxi.get_player().vitals.tp
    if ignore_conditions and tp >= player_tp then
        return true
    end
    local target = windower.ffxi.get_mob_by_target("t")
    if target == nil or target.hpp < hpp_threshold then
        return false
    end
    return false
end

function check_combat()
    local player = windower.ffxi.get_player()
    if not player.in_combat then
        disengage()
    end
end

function disengage()
    if engaged then
        log("disengaged")
    end
    engaged = false
end

function check_for_trusts()
    -- we need new references to these every time
    -- because this is a copy; stats don't update
    clear_tables()
    local get_party = windower.ffxi.get_party()
    -- for i=1,5 do
    for i=1,3 do
        if get_party['p' .. i] == nil then
            break
        end
        trust_name = get_party['p' .. i].name
        -- log('name is ' .. trust_name)
        if touchy_trusts:containskey(trust_name) then
            log(trust_name .. ' is in touchy trusts')
            -- log("found a touchy trust: " .. trust_name)
            -- something weird here if we create a local far and do get_party
            -- think it's reusing the same object, so last trust is only registered trust
            -- so we just read the table each time ;(
            table.insert(found_touchy, get_party['p' .. i])

            -- find other melee types who will chain ws
                -- shantotto II, ArkEV, Zeid II
            -- find other melee types who will spam ws at 1k
                -- qultada
            -- find other melee types who will ws when player has tp
                -- ayame
            -- break
        elseif sane_trusts:containskey(trust_name) then
            -- log(trust_name .. ' is in sane trusts')
            -- log("found a sane trust: " .. trust_name)
            table.insert(found_sane, get_party['p' .. i])
        end
    end

    -- for testing
    -- for k,v in pairs(found_touchy) do
    --     log("found " .. v.name)
    -- end
    -- for k,v in pairs(found_sane) do
    --     log("found " .. v.name)
    -- end
end

function clear_tables()
    -- log('clearing tables')
    for k,v in pairs(found_touchy) do found_touchy[k] = nil end
    for k,v in pairs(found_sane) do found_sane[k] = nil end
end

function process_cycle()
    -- don't ws if touchy trust is about to
    if touchy_trust_ready() then
        return
    end
    if touchy_trust_pending() or sane_trust_ready() or ignore_conditions then
        -- but not if another melee type also has TP; they will screw up the SC
        chat(122,'Good to WS')
        execute_ws()
    end
end

function touchy_trust_ready()
    for k,v in pairs(found_touchy) do
        if v.tp > 1400 then
            chat(122, v.name .. ' has tp: ' .. v.tp)
            return true
        end
    end
    return false
end

function touchy_trust_pending()
    for k,v in pairs(found_touchy) do
        if v.tp > 805 then
            chat(122, v.name .. ' has tp: ' .. v.tp)
            return true
        end
    end
    return false
end

function sane_trust_ready()
    for k,v in pairs(found_sane) do
        if v.tp > 805 then
            chat(122, v.name .. ' has tp: ' .. v.tp)
            return true
        end
    end
    return false
end

function execute_ws()
    -- if berserk then
    --     windower.send_command('input /ja "Berserk" <me>; wait 1; input /ja "Warcry" <me>')
    --     coroutine.sleep(2)
    -- end
    -- if trick_attack then
    --     windower.send_command('input /ja "Trick Attack" <me>; wait 1; input /ws "' .. ws .. '" <t>')
    -- else
    --     windower.send_command('input /ws "' .. ws .. '" <t>')
    -- end
    chat(122,'table size: ' .. #ws_list .. '.')

    if chain and #ws_list > 0 then
        windower.send_command('input /ws "' .. ws_list[0] .. '" <t>')
        coroutine.sleep(sleep_time)
        while (windower.ffxi.get_player().vitals.tp < 1000) do
            coroutine.sleep(sleep_time)
        end
        windower.send_command('input /ws "' .. ws_list[1] .. '" <t>')
    else
        windower.send_command('input /ws "' .. ws_list[0] .. '" <t>')
    end
end

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = {...}
    if command1 == 'run' or command1 == 'start' then
        run_it()
    elseif command1 == 'stop' then
        stop_it()
    elseif command1 == 'ws1' then
        ws_input = table.concat(cmd_remainder, ' ')
        if ws_input == 'get' or ws_input == 'show' then
            chat(122,'Selected WS: ' .. ws_list[0] .. '.')
        else
            -- ws = ws_input
            ws_list[0] = ws_input
            chat(122,'WS1 set to: ' .. ws_list[0] .. '.')
        end
    elseif command1 == 'ws2' then
        ws_input = table.concat(cmd_remainder, ' ')
        if ws_input == 'get' or ws_input == 'show' then
            chat(122,'Selected WS: ' .. ws_list[1] .. '.')
        else
            ws_list[1] = ws_input
            chat(122,'WS2 set to: ' .. ws_list[1] .. '.')
        end
    elseif command1 == 'hp' then
        hpp_input = table.concat(cmd_remainder, ' ')
        hpp_threshold = tonumber(hpp_input)
        chat(122,'HP threshold set to: ' .. hpp_input .. '.')
    elseif command1 == 'tp' then
        tpp_input = table.concat(cmd_remainder, ' ')
        sane_threshold = tonumber(tpp_input)
        chat(122,'Sane trusts TP threshold set to: ' .. tpp_input .. '.')
    elseif command1 == 'mytp' then
        ptpp_input = table.concat(cmd_remainder, ' ')
        player_tp = tonumber(ptpp_input)
        chat(122,'Player TP threshold set to: ' .. player_tp .. '.')
    elseif command1 == 'ta' then
        trick_attack = not trick_attack
        chat(122,'Trick Attack set to: ' .. tostring(trick_attack) .. '.')
    elseif command1 == 'fast' then
        ignore_conditions = not ignore_conditions
        chat(122,'Ignore conditions is set to: ' .. tostring(ignore_conditions) .. '.')
    elseif command1 == 'big' then
        ws = "Savage Blade"
        trick_attack = true
        berserk = true
        player_tp = 3000
        chat(122,'Big boom time')
    elseif command1 == 'test' then
        log('checking for trusts')
        check_for_trusts()
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    if engaged then
        if string.match(original, "is out of range") or string.match(original, "Unable to see the") then
            disengage()
        end
    else
        if string.match(original, player_name .. " hits") or string.match(original, player_name .. " scores a") then
            log("engaged")
            engaged = true
        end
    end
end)

-- run_it()
