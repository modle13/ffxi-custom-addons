_addon.name = 'My Rings User'
_addon.author = 'someguy'
_addon.version = '1.0'
_addon.commands = {'rings'}

require('logger')
extdata = require('extdata')
resources = require('resources')

lang = string.lower(windower.ffxi.get_info().language)
capacity_item_info = {
    [1]={id=15199,japanese='idk',english='"Guide Beret"'     , slot=4 },
    [2]={id=28469,japanese='idk',english='"Endorsement Ring"', slot=13},
    [3]={id=27557,japanese='idk',english='"Trizek Ring"'     , slot=13},
    [4]={id=28546,japanese='idk',english='"Capacity Ring"'   , slot=13},
    [5]={id=26165,japanese='idk',english='"Facility Ring"'   , slot=13},
}

exp_item_info = {
    [1]={id=26164,japanese='idk',english='"Caliber Ring"'   , slot=13},
    [2]={id=28568,japanese='idk',english='"Resolution Ring"', slot=13},
    [3]={id=27556,japanese='idk',english='"Echad Ring"'     , slot=13},
    [4]={id=15762,japanese='idk',english='"Empress Band"'   , slot=13},
    [5]={id=15198,japanese='idk',english='"Sprout Beret"'   , slot=4 },
}

local target_set = capacity_item_info

function determine_set_type(type_to_set)
    if type_to_set == 'exp' or type_to_set == 'xp' or tonumber(windower.ffxi.get_player().main_job_level) < 99 then
        target_set = exp_item_info
        log('setting item type to exp')
    else
        target_set = capacity_item_info
        log('setting item type to capacity')
    end
end

function search_item(check_flag)
    local item_array = {}
    -- 1 is safe, 2 is storage
    local bags = {0,8,10,11,12} --inventory,wardrobe1-4
    local get_items = windower.ffxi.get_items
    for i=1,#bags do
        for _,item in ipairs(get_items(bags[i])) do
            -- -- to determine item id
            -- if item.id ~= 0 then
            --     local item_name = resources.items[tonumber(item.id)].name
            --     log(item_name .. ' ' .. item.id)
            -- end
            if item.id > 0 then
                item_array[item.id] = item
                item_array[item.id].bag = bags[i]
            end
        end
    end
    -- do return end -- to exit early when getting item ids
    for index, stats in pairs(target_set) do
        local item = item_array[stats.id]
        local set_equip = windower.ffxi.set_equip
        if item then
            local ext = extdata.decode(item)
            local enchant = ext.type == 'Enchanted Equipment'
            local recast = enchant and ext.charges_remaining > 0 and math.max(ext.next_use_time + 18000 - os.time(), 0)
            local usable = recast and recast == 0
            log(stats[lang], usable and '' or recast and recast .. ' sec recast.')
            if usable or ext.type == 'General' then
                if check_flag and check_flag == 'check' then
                    log(stats[lang], delay)
                    break
                end
                if enchant and item.status ~= 5 then --not equipped
                    set_equip(item.slot, stats.slot, item.bag)
                end
                log_flag = true
                repeat --waiting cast delay
                    coroutine.sleep(1)
                    local ext = extdata.decode(get_items(item.bag, item.slot))
                    local delay = ext.activation_time+18000-os.time()
                    if delay > 0 then
                        log(stats[lang], delay)
                    elseif log_flag then
                        log_flag = false
                        log('Item use within 3 seconds...')
                    end
                until ext.usable or delay > 30
                windower.send_command('input /item ' .. windower.to_shift_jis(stats[lang]) .. ' <me>')
                break;
            end
        else
            log('You don\'t have '..stats[lang]..'.')
        end
    end
end

windower.register_event('addon command',function(command1, command2, ...)
    local args = T{...}
    if command1 == 'set' then
        determine_set_type(command2)
    else
        search_item(command1)
    end
end)
