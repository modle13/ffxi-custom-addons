_addon.name = 'MySunbreeze'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'mysun'}

require('logger')

local emotes = {"clap", "cheer", "wave"}
local running = false

windower.register_event('incoming text', function(original, new, color)
    if running then
        do return end
    end
    if string.match(original, "Mumor : Shining Summer Samba") then
        windower.send_command('input /dance1 motion')
    elseif string.match(original, "Mumor : Lovely Miracle Waltz") then
        windower.send_command('input /dance2 motion')
    elseif string.match(original, "Mumor : Neo Crystal Jig") then
        windower.send_command('input /dance3 motion')
    elseif string.match(original, "Mumor : Super Crusher Jig") then
        windower.send_command('input /dance4 motion')
    end
end)

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = table.concat({...}, ' ')
    if command1 == 'clap' then
        running = not running
        log('operation running? ' .. tostring(running))
        spam_emotes()
    else
        log('command is bad: "' .. command1 .. '"')
    end
end)

function spam_emotes()
    while running do
        for k,v in pairs(emotes) do
            windower.send_command('input /' .. v .. ' motion')
            coroutine.sleep(1)
        end
    end
end