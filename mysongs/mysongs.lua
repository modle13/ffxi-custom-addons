_addon.name = 'My Songs'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'songs'}

require('logger')

-- create a set with input string
-- //buff create myset
-- take an input string as a buff name
-- add it to a list
-- //buff add mybuff

function showsongs()
    local player = windower.ffxi.get_player()
    buffs = player["buffs"]
    for k,v in pairs(buffs) do
        log (k .. ': ' .. v)
    end
end

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower()
    if command1 == 'help' then
        print('%s v%s':format(_addon.name, _addon.version))
        print('    \\cs(255,255,255)no help for you\\cr - yo')
    else
        showbuffs()
    end
end)
