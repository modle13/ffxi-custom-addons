_addon.name = 'AutoPassLead'
_addon.author = 'sapcehamster'
_addon.commands = {'autopass','ap'} -- check for conflicts with other addons
_addon.version = '0.1.0'
_addon.language = 'english'

-- luau contains the config loader
require('luau')
require('logger')

-- listen for pass request
-- /t current_leader pass 
-- to pass leader to me
-- or
-- /t current_leader pass other_member

-- big problems to solve
-- 1. listen for a tell, capture sender
-- 

------- features
-- 1. tell pass lead to target char, target char passes lead to asker
--    respond with target not in part if not in party
-- 2. tell `pass lead other_member`, target char passes lead to target player
--    respond with leader not in party if not in party
--    respond with player not in party if not in party

-- addon commands would be for setting configuration
windower.register_event('addon command', function(command1, command2, ...)
    command1 = command1 and command1:lower() or 'help'
    cmd_remainder = table.concat({...}, ' ')
    -- look at autoinvite whitelist aliasing
    -- if it's not in autoinvite, look in autojoin
    if command1 == 'whitelist' then
        if command2 == 'add' then
            log('whitelisting ' .. cmd_remainder)
        elseif command2 == 'remove' then
            log('removing ' .. cmd_remainder .. 'from whitelist')
        end
    end
end)

-- incoming tell? ChatMon does this kind of stuff
--windower.register_event('incoming text', function(original, new, color)
--    log('requested to pass lead to')
-- -- if not lead, respond with i'm not a lead
--end)

settings = config.load(defaults)
log(settings)
log(settings.autodecline)
log(settings.mode)
