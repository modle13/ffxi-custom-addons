_addon.name = 'Unity Helper'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'unity', 'un'}

require('logger')
require('coroutine')
packets = require('packets')

local chat = windower.add_to_chat

local running = false

local handlers = {}
local conditions = {
    receive = false,
    box = false,
    rift = false,
    escape = false,
    trade = true,
    received = false,
}

local found_target = nil

local fight_target_id = 16793895
local fight_target_name = 'Intuila'

function reset_conditions()
    for k,v in pairs(conditions) do
        conditions[k] = false
    end
end

function engage()
    windower.send_command('input //settarget ' .. fight_target_id)
    windower.send_command('input /attack <t>')
    if conditions["fighting"] and not conditions["engaged"] then
        coroutine.schedule(engage, 1)
    end
end

--This command handles popping once a junction is found, using setkey means user can't enter any inputs while it's running.
--TODO It would be nice to handle menuing via packets, look into
function poke()
    conditions['poking'] = true
    windower.send_command('setkey enter down')
    coroutine.sleep(0.5)
    windower.send_command('setkey enter up')
    coroutine.sleep(1.5)

    windower.send_command('setkey up down')
    coroutine.sleep(0.5)
    windower.send_command('setkey up up')
    coroutine.sleep(0.5)

    windower.send_command('setkey enter down')
    coroutine.sleep(0.5)
    windower.send_command('setkey enter up')
    coroutine.sleep(0.5)

    windower.send_command('setkey up down')
    coroutine.sleep(0.5)
    windower.send_command('setkey up up')
    coroutine.sleep(0.5)

    windower.send_command('setkey enter down')
    coroutine.sleep(0.5)
    windower.send_command('setkey enter up')
    coroutine.sleep(0.5)

    --Confirm successful poke (Or succesful completion of poke commands, can be interrupted by user input without unm knowing)
    windower.add_to_chat(167, 'Incoming! Poke successful')
    --Targeting should be done by EasyFarm or Scripted, this won't auto fight for you
    --windower.chat.input("/targetnpc") //if you want auto engage code this is where you would put it
    coroutine.sleep(5)
    conditions['poking'] = false
    conditions['junction'] = false
end

local function do_the_thing()
    if not conditions['poking'] then
        chat(125, _addon.name .. ' found target ' .. found_target.name)
        target_command = 'input //settarget ' .. found_target.id
        windower.send_command(target_command)
        poke()
    end
end

local function observe_junction_spawn(id, data)
    if (id == 0xe) and running and not conditions['junction'] then
        local p = packets.parse('incoming', data)
        local npc = windower.ffxi.get_mob_by_id(p['NPC'])
        if not npc then elseif (npc.name == 'Ethereal Junction') then
            found_target = npc
            log('junction spawn')
            conditions['junction'] = true
            coroutine.schedule(do_the_thing, 1)
        end
    end
end

local function start()
    running = true
end

local function stop()
    running = false
end

handlers['start'] = start
handlers['stop'] = stop

local function handle_command(...)
    local cmd  = (...) and (...):lower()
    local args = {select(2, ...)}
    if handlers[cmd] then
        local msg = handlers[cmd](unpack(args))
        if msg then
            error(msg)
        end
    else
        error("unknown command %s":format(cmd))
    end
end

windower.register_event('addon command', handle_command)
windower.register_event('incoming chunk', observe_junction_spawn)

windower.register_event('incoming text', function(original, new, color)
    original = original:strip_format()
    if string.match(original, "have completed the following") then
        reset_conditions()
        log("done")
    end
    if string.match(original, "Poke successful") then
        log("starting")
        conditions["fighting"] = true
    end
    if string.match(original, "hits " .. fight_target_name .. " for") and not conditions["engaged"] then
        log("engaged")
        conditions["engaged"] = true
    end
end)
