return {
    "Aoidos' Earring",
    "Ayanmo Gambieras",
    "Ayanmo Corazza +2",
    "Ayanmo Cosciales +1",
    "Ayanmo Manopolas +1",
    "Ayanmo Zucchetto +1",
    "Ayanmo Ring",
    "Brioso Cannions +2",
    "Brioso Slippers +3",
    "Brioso Justau. +2",
    "Brioso Roundlet +2",
    "Brioso Cuffs +2",
    "Bihu Cannions",
    "Bihu Cuffs",
    "Bihu Slippers",
    "Bihu Roundlet",
    "Bihu Justaucorps",
    "Eletta Knife",
    "Fili Cothurnes +1",
    "Fili Rhingrave +1",
    "Fili Calot +1",
    "Fili Hongreline +1",
    "Fili Manchettes +1",
    "Inyan. Crackows +1",
    "Inyanga Dastanas",
    "Inyanga Jubbah",
    "Inyanga Ring",
    "Inyanga Shalwar",
    "Inyanga Tiara",
    "Loquac. Earring",
    "Twilight Torque",
    "Loquacious Earring",
}
