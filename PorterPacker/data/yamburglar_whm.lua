return {
    "Theophany Cap",
    "Theophany Mitts",
    "Theo. Bliaut",
    "Theo. Duckbills",
    "Theo. Pantaloons",
    "Inyanga Tiara +1",
    "Inyanga Jubbah",
    "Inyan. Dastanas +1",
    "Inyan. Crackows +1",
    "Angantyr Beret",
    "Angantyr Robe",
    "Ebers Cap +1",
    "Ebers Bliaut +1",
    "Ebers Mitts +1",
    "Ebers Pantaloons +1",
    "Ebers Duckbills +1",
    "Ayanmo Zucchetto +1",
    "Ayanmo Corazza +2",
    "Ayanmo Manopolas +2",
    "Ayanmo Cosciales +1",
    "Aya. Gambieras +1",
    "Ayanmo Ring",
}