return {
    "Ayanmo Zucchetto",
    "Ayanmo Corazza +2",
    "Ayanmo Manopolas",
    "Ayanmo Cosciales +1",
    "Ayanmo Gambieras",
    "Ayanmo Ring",
    "Ebers Bliaud",
    "Ebers Cap",
    "Ebers Duckbills",
    "Ebers Mitts",
    "Ebers Pantaloons",
    "Inyan. Crackows +2",
    "Inyan. Dastanas +1",
    "Inyanga Jubbah +1",
    "Inyanga Ring",
    "Inyanga Shalwar +1",
    "Inyanga Tiara +2",
    "Loquac. Earring",
    "Orison Earring",
    "Orison Locket",
    "Piety Briault +1",
    "Piety Cap",
    "Piety Pantaloons",
    "Theo. Cap +1",
    "Theo. Duckbills +2",
    "Theo. Mitts +1",
    "Theo. Briault",
    "Twilight Torque",
}
