return {
    "Ayanmo Zucchetto +1",
    "Ayanmo Corazza +2",
    "Ayanmo Manopolas +2",
    "Ayanmo Cosciales +1",
    "Aya. Gambieras +1",
    "Ayanmo Ring",
    "Jhakri Coronal +2",
    "Jhakri Robe +2",
    "Jhakri Cuffs +2",
    "Jhakri Slops +2",
    "Jhakri Pigaches +2",
    "Jhakri Ring",
    "Loquac. Earring",
    "Naegling",
    "Maxentius",
    "Eletta Rod",
    "Twilight Torque",
    "Twilight Cloak",
    "Vitiation Chapeau",
    "Vitiation Tabard",
    "Vitiation Gloves",
    "Vitiation Tights",
    "Lethargy Chappel +1",
    "Lethargy Sayon +1",
    "Lethargy Gantherots +1",
    "Lethargy Fuseau +1",
    "Lethargy Houseaux +1",
    "Atrophy Chapeau +1",
    "Atrophy Tabard +1",
    "Atrophy Gloves +1",
    "Atrophy Tights +1",
    "Atrophy BOots +1",
    "Estoqueur's Cape",
}
