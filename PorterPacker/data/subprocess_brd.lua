return {
    "Aoidos' Earring",
    "Ayanmo Zucchetto +1",
    "Ayanmo Corazza +2",
    "Ayanmo Manopolas +2",
    "Ayanmo Cosciales +1",
    "Ayanmo Gambieras +1",
    "Ayanmo Ring",
    "Brioso Cannions +2",
    "Brioso Slippers +3",
    "Brioso Justau. +2",
    "Brioso Roundlet +2",
    "Brioso Cuffs +2",
    "Bihu Cannions",
    "Bihu Cuffs",
    "Bihu Slippers",
    "Bihu Roundlet",
    "Bihu Justaucorps",
    "Eletta Knife",
    "Fili Cothurnes +1",
    "Fili Rhingrave +1",
    "Fili Calot +1",
    "Fili Hongreline +1",
    "Fili Manchettes +1",
    "Inyan. Crackows +1",
    "Inyan. Dastanas",
    "Inyanga Jubbah",
    "Inyanga Ring",
    "Inyanga Shalwar +1",
    "Inyanga Tiara +1",
    "Loquac. Earring",
    "Twilight Torque",
    "Loquacious Earring",
}
