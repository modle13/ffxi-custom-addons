return {
    "Inyan. Crackows +2",
    "Inyan. Dastanas +2",
    "Inyanga Jubbah +1",
    "Inyanga Ring",
    "Inyanga Shalwar +2",
    "Inyanga Tiara +2",
    "Loquac. Earring",
    "Twilight Torque",
    "Beckoner's Bracers +1",
    "Beckoner's Horn +1",
    "Beckoner's Spats +1",
    "Beckoner's Doublet +1",
    "Beckoner's Pigaches +1",
    "Convoker's Horn +1",
    "Convoker's Doublet +1",
    "Convoker's Bracers +1",
    "Convoker's Spats +1",
    "Convoker's Pigaches +1",
    "Caller's Earring",
    "Glyphic Bracers +1",
    "Glyphic Horn +1",
    "Glyphic Pigaches +1",
    "Glyphic Spats +1",
    "Glyphic Doublet +1",
    -- "Summoner's*",
    -- "Evoker's*",
    -- "Caller's*"
}
