di = {}

local last_di_tell = os.time()

function di:run(cmd, who)
    who = who or ''
    log("running di command")
    if cmd == nil then
        if os.time() - last_di_tell > 60 then
            execute('/tell whereisdi /')
            last_di_tell = os.time()
        end
    elseif cmd == 'ring' then
        if who ~= '' then
            log("telling " .. who .. " to equip DI ring")
            execute('//send @' .. who .. ' //a di ring')
        else
            log("equipping DI ring")
            execute('/equip ring1 "Dim. Ring (Dem)"')
        end
    elseif T{'1', '2', '3'}:contains(cmd) then
        di:warp_to_zone(cmd, who)
        if cmd == '3' then
            wait_for(40)
        else
            wait_for(30)
        end
        execute('//a di go')
    elseif cmd == 'go' then
        local currentzone = windower.ffxi.get_info()['zone']
        log("current zone is " .. currentzone)
        local result = di:do_zone(currentzone, who)
    else
        log('incorrect params; usage: "//a di [go,ring,1,2,3] [all]')
    end
end

function di:warp_to_zone(cmd, who)
    if cmd == '1' then
        -- HP to qufim
        execute('//hp ' .. who .. ' qufim')
    elseif cmd == '2' then
        -- HP to misar coast
        execute('//hp ' .. who .. ' misar')
    elseif cmd == '3' then
        -- dim ring to reisenjima
        local target = windower.ffxi.get_player().name
        if who ~= '' then
            target = '@' .. who
        end
        execute('//send ' .. target .. ' //port')
        wait_for(5)
    end
end

function di:do_zone(zone, who)
    if who ~= '' then
        -- recursively redirects to all characters using send
        execute('//send @' .. who .. ' //a di go')
        return
    end
    if T{126, 25, 117, 108, 102}:contains(zone) then
        -- zones with escha entrances
        execute('//ew enter')
        if zone == 126 then
            -- qufim
            run_for(3, 1.45)
        elseif zone == 25 then
            -- misar
            run_for(3.5, -0.74)
        else
            -- then this is a crag zone, just run forward
            run_for(2)
        end
        wait_for(20)
        execute('//a di go')
    elseif T{288, 289, 291}:contains(zone) then
        -- escha zones
        execute('//ew domain')
        if zone == 288 then
            -- escha zitah
            run_for(3, -2.54)
        elseif zone == 289 then
            -- escha ruaun
            run_for(3, -2.5)
        end
        wait_for(10)
        execute('//cancel mobilization')
        -- run north 3 seconds
        run_for(3, -1.57)
    end
end

function target_npc(target_id)
    log("targeting " .. target_id)
    execute('//settarget ' .. target_id)
    coroutine.sleep(2)
    execute('/lockon')
end

function run_for(duration, direction)
    -- 0 is east, pi/2 south, -pi or pi is west, -pi/2 is north
    -- N  -1.57
    -- NW -2.25
    -- W   3.14
    -- SW  2.25
    -- S   1.57
    -- SE  0.75
    -- E   0
    -- NE -0.75


    if direction ~= nil then
        windower.ffxi.run(direction)
    else
        windower.ffxi.run()
    end
    coroutine.sleep(duration)
    windower.ffxi.run(false)
end

function run_to(x, y)
    -- repeat
    --     me = windower.ffxi.get_mob_by_target('me')
    --     windower.ffxi.run(x - me.x, 0, y - me.y)
    --     coroutine.sleep(.0005)
    -- until x - me.x < 3 and y - me.y < 3
    windower.ffxi.run(false)
end

function wait_for(duration)
    coroutine.sleep(duration)
end

function execute(command)
    windower.send_command('input ' .. command)
end