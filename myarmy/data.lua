data = {}

data.activities = {
    ["Chihemihe"] = {
        ["ambu"]   = {["main"] = "THF", ["sub"] = "WAR"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    ["Lauralanthalesa"] = {
        ["ambu"]   = {["main"] = "COR", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "COR", ["sub"] = "BLM"},
    },
    ["Cheeheemeehee"] = {
        ["ambu"]   = {["main"] = "RDM", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    ["Tikamajere"] = {
        ["ambu"]   = {["main"] = "RDM", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "RDM", ["sub"] = "BLM"},
    },
    -- account 2
    ["Kitiaramatar"] = {
        ["ambu"]   = {["main"] = "BRD", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    ["Crowmane"] = {
        ["ambu"]   = {["main"] = "BRD", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BRD", ["sub"] = "WHM"},
    },
    ["Subprocess"] = {
        ["ambu"]   = {["main"] = "BRD", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    ["Turtleburglar"] = {
        ["ambu"]   = {["main"] = "BRD", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    -- account 3
    ["Yamburglar"] = {
        ["ambu"]   = {["main"] = "COR", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "BLU", ["sub"] = "RDM"},
    },
    ["Boatmeal"] = {
        ["ambu"]   = {["main"] = "RDM", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "COR", ["sub"] = "BLM"},
    },
    ["Undresatndalbe"] = {
        ["ambu"]   = {["main"] = "COR", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "COR", ["sub"] = "BLM"},
    },
    ["Maptakenback"] = {
        ["ambu"]   = {["main"] = "COR", ["sub"] = "NIN"},
        ["coal"] =   {["main"] = "RDM", ["sub"] = "BLM"},
        ["sparks"] = {["main"] = "COR", ["sub"] = "BLM"},
    },
}

data.characters = T{
    'chihemihe',
    'lauralanthalesa',
    'cheeheemeehee',
    'tikamajere',
    'kitiaramatar',
    'crowmane',
    'subprocess',
    'turtleburglar',
    'yamburglar',
    'boatmeal',
    'undresatndalbe',
    'maptakenback',
}

data.coalition_warp_map = T{
    ["0"] = "western adoulin auction", -- western adoulin auction
    ["1"] = "western adoulin 2", -- pioneer; it's 1 because that's where the HP is
    ["2"] = "western adoulin 1", -- courier
    ["3"] = "western adoulin 3", -- mummer
    ["4"] = "western adoulin 4", -- inventor
    ["5"] = "eastern adoulin 1", -- peacekeeper
    ["6"] = "eastern adoulin 2", -- scout
    ["wm"] = "western adoulin mog", -- western adoulin mog
    ["em"] = "eastern adoulin mog", -- western auction
    ["wa"] = "western adoulin auction", -- western adoulin auction
    ["ea"] = "eastern adoulin auction", -- easter adoulin auction
    ["c1"] = "ceiz 1", -- ceizak bg biv 1
    ["c2"] = "ceiz 2", -- ceizak bg biv 2
    ["cf"] = "ceiz fs", -- ceizak bg frontier station
    ["f1"] = "foret 1", -- foret biv 1
    ["f2"] = "foret 2", -- foret biv 2
    ["f3"] = "foret 3", -- foret biv 3
    ["f4"] = "foret 4", -- foret biv 4
    ["ff"] = "foret fs", -- foret frontier station
    ["m1"] = "morim 1", -- morim biv 1
    ["m2"] = "morim 2", -- morim biv 2
    ["m3"] = "morim 3", -- morim biv 3
    ["m4"] = "morim 4", -- morim biv 4
    ["m5"] = "morim 5", -- morim biv 5
    ["mf"] = "morim fs", -- morim frontier station
    ["ma1"] = "marj 1", -- marj biv 1
    ["ma1"] = "marj 1", -- marj biv 1
    ["ma2"] = "marj 2", -- marj biv 2
    ["ma3"] = "marj 3", -- marj biv 3
    ["ma4"] = "marj 4", -- marj biv 4
    ["maf"] = "marj fs", -- marj frontier station
    ["y1"] = "yorc 1", -- yorc biv 1
    ["y2"] = "yorc 2", -- yorc biv 2
    ["yf"] = "yorc fs", -- yorc frontier station
}