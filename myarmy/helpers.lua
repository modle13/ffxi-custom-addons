res = require('resources')

function has_buff_by_name(target_buff)
    local buff_active = false
    local player_buffs = windower.ffxi.get_player()["buffs"]
    local target_buff_id = res.buffs:with('name', target_buff).id

    log('checking for ' .. target_buff .. ' with target id ' .. target_buff_id)

    for k,v in pairs(player_buffs) do
        if v == tonumber(target_buff_id) then
            buff_active = true
            log('buff already active : ' .. target_buff)
            break
        end
    end
    if not buff_active then
        log('BUFF NOT ACTIVE : ' .. target_buff)
    end
    return buff_active
end

function showbuffs()
    local player = windower.ffxi.get_player()
    buffs = player["buffs"]
    for k,v in pairs(buffs) do
        local buff_name = res.buffs[v].en
        log (k .. ': ' .. v .. ': ' .. buff_name)
    end
end

function print_table(data_table)
    log(data_table)
    for k,v in pairs(data_table) do
        if type(v) == 'table' then
            for k2,v2 in pairs(v) do
                log (k2 .. ': ' .. tostring(v2))
            end
        else
            log (k .. ': ' .. tostring(v))
        end
    end
end

function get_my_data()
    local me = windower.ffxi.get_mob_by_target('me')
    print_table(me)
end
