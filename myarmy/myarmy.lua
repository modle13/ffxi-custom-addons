_addon.name = 'MyArmy'
_addon.author = 'Guy'
_addon.version = '0.0.0'
_addon.commands = {'army', 'ar', 'a'}

require('logger')

require('data')
require('di')
require('helpers')

-- to add:
-- join
-- follow
-- load myrings, superwarp, porterpacker
-- //army wp westadou #
-- coalition wp warp shortcuts
-- sellnpc common items like garudite etc
-- auto char switch/login?
-- hp/sg anywhere: warp, hp tavn, hp/sg



local di_zone_map = T{
    
}

function addons(flag)
    action = 'reload'
    if flag == 'off' then
        action = 'unload'
    end
    windower.send_command('input //send @all //lua reload myarmy')
    windower.send_command('input //send @all //lua ' .. action .. ' cancel')
    windower.send_command('input //send @all //lua ' .. action .. ' myrings')
    windower.send_command('input //send @all //lua ' .. action .. ' myporter')
    windower.send_command('input //send @all //lua ' .. action .. ' porterpacker')
    windower.send_command('input //send @all //lua load superwarp')
    log('reloaded common addons')
end

function ambu(all)
    all = all or ''
    windower.send_command('input //a gear ambu jc ' .. all)
    coroutine.sleep(5)
    follow()
    coroutine.sleep(5)
    windower.send_command('input //a gear ambu po ' .. all)
end

-- assist and attack
function attack(flag)
    if flag == 'off' then
        windower.send_command('input //send @others /attack off')
    else
        windower.send_command('input //send @others /assist ' .. windower.ffxi.get_player().name)
        windower.send_command('input //send @others /attack on')
        coroutine.sleep(1.5)
        windower.send_command('input //send @others /attack on')
    end
end

-- myautobattle
function autobattle(flag)
    action = 'load'
    if flag == 'off' then
        action = 'unload'
    end
    windower.send_command('input //send @all //lua ' ..  action .. ' myautobattle')
    log('autobattle ' .. action)
end

function buffs(cmd)
    -- print_table(res.buffs[627])
    showbuffs()
end

-- handle coaltion movement
function coalition_warp(target, who)
    target = target or 1
    who = "@" .. (who or '')
    log('getting ' .. target .. ' from co warp map')
    destination = data.coalition_warp_map[target]
    full_command = "input //wp " .. who .. " " .. destination
    log('warping to ' .. destination .. ' command is ' .. full_command)
    windower.send_command(full_command)
end

function cp_cape(all_or_other)
    if all_or_other ~= nil then
        windower.send_command('input //send @' .. all_or_other .. ' //a cp')
        -- windower.send_command('input //send @' .. all_or_other .. ' /equip back \\"Mecisto. Mantle\\"')
    else
        log("equipping the thing")
        windower.send_command('input //gs disable back')
        windower.send_command('input /equip back "Mecisto. Mantle"')
    end
end

function debug()
    -- log(has_buff_by_name('Refresh'))
    -- log(has_buff_by_name('Mobilization'))
    log(windower.ffxi.get_position())
    -- print_table(windower.ffxi.get_info())

    me = windower.ffxi.get_mob_by_target('me')
    log(me.facing)
    -- print_table(me)
    -- run_for(3, -0.74)
    -- run_for(2, -2.5)
    local currentzone = windower.ffxi.get_info()['zone']
    log(currentzone)


    -- tokens = T{}
    -- for token in string.gmatch(windower.ffxi.get_info(), "[^%s]+") do
    --     log(token)
    --     tokens[token] = true
    -- end
    -- print_table(tokens)
    -- windower.ffxi.run(3.14)
end

function domain_invasion(cmd, who)
    di:run(cmd, who)
end

function fast(all)
    local job = windower.ffxi.get_player().main_job:lower()
    local command = ''
    if job == 'brd' then
        command = '/ma chocobo mazurka'
    elseif job == 'cor' then
        command = "/ja bolter's roll"
    elseif job == 'smn' then
        -- whatever garuda's thing is, fleet something
        command = ''
    elseif job == 'dnc' then
        -- whatever the dnc thing is
        command = ''
    end
    if all ~= nil then
        windower.send_command('input //send @others //a fast')
    end
    if command ~= '' then
        windower.send_command('input ' .. command)
    end
end
    
-- follow current
function follow()
    windower.send_command('input //send @others /follow ' .. windower.ffxi.get_player().name)
end

-- gear up for whatever
function gear_up(activity, action, all)
    if activity == nil or action == nil then
        log('incorrect params; usage: "//a gear [ambu,sparks] [jc,po] [all]')
        do return end
    end
    local player = windower.ffxi.get_player().name
    player_table = data.activities[player]
    if player_table == nil then
        log("player " .. player .. " not configured in 'activites' table")
        do return end
    end
    activity_jobs = player_table[activity]
    if activity_jobs == nil then
        log("player " .. player .. " does not have activity '" .. activity .. "' configured in 'activites' table")
    end
    if action == 'jc' then
        log("player changing to " .. activity_jobs.main .. "/" .. activity_jobs.sub)
        command = '//jc ' .. activity_jobs.main:lower() .. '/' .. activity_jobs.sub:lower()
    elseif action == 'po' then
        command = '//po swap'
    end
    windower.send_command('input ' .. command)
    if all ~= nil then
        windower.send_command('input //send @others //a gear ' .. activity .. ' ' .. action)
    end
end

function invisible()
    windower.send_command('input //send @all //ma invisible me')
end

-- follow current
function invite()
    for i, v in pairs(data.characters) do
        log('inviting ' .. v)
        windower.send_command('input /pcmd add ' .. v)
        coroutine.sleep(1)
    end
end

-- logout all
function logout()
    windower.send_command('input //send @all /logout')
end

-- lullaby bt
function lullaby()
    windower.send_command('input //send @brd /ma horde lullaby 2 bt')
    coroutine.sleep(1)
    windower.send_command('input //send @brd /ma horde lullaby 2 t')
end

-- warp to bastok mines to spend merits/job points
function merits()
    windower.send_command('input //hp bastok mines mog')
end

-- mount up
function mount(flag)
    if flag == 'off' then
        windower.send_command('input //send @all /dismount')
        coroutine.sleep(2)
        windower.send_command('input //send @others /follow ' .. windower.ffxi.get_player().name)
    else
        windower.send_command('input //send @others /follow ' .. windower.ffxi.get_player().name)
        windower.send_command('input //send @all /ja red crab')
        windower.send_command('input //send @all /ja raptor')
        -- coroutine.sleep(2)
    end
end

-- porter packer
function porter(flag)
    action = 'swap'
    if flag == 'pack' then
        action = 'pack'
    end
    windower.send_command('input //send @all //po ' .. action)
end

-- use set repeater command
function repeater(delay, action, target, all)
    if (not (delay == 'off' or delay == 'stop') or delay == nil) and (action == nil or target == nil) then
        log('wrong params, expected: delay action target [all]')
        do return end
    end
    if delay == 'off' or delay == 'stop' then
        windower.send_command('input //lua unload repeater')
    else
        windower.send_command('input //lua reload repeater')
        coroutine.sleep(1)
        if all ~= nil then
            windower.send_command('input //rpt command input //send @all /ma \"' .. action .. '\" ' .. target)
        else
            windower.send_command('input //rpt command input /ma \"' .. action .. '\" ' .. target)
        end
        windower.send_command('input //rpt delay ' .. delay)
        windower.send_command('input //rpt start')
        log('set repeater command ' .. action .. ' @' .. target .. '; delay ' .. delay .. ' for all? ' .. all)
    end
end

-- use xp/cp rings
function rings()
    windower.send_command('input //send @all //rings')
end

-- shutdown all
function shutdown()
    windower.send_command('input //send @all /shutdown')
end

function sneak()
    windower.send_command('input //send @all //ma sneak me')
end

function sparks(flag, secondary)
    addons = T{'sparks', 'unitynpc', 'sellnpc'}
    sell_items = T{'acheron shield', 'ice shield', 'prize powder'}
    action = flag
    if flag == 'off' then
        action = 'unload'
    elseif flag == nil then
        action = 'reload'
    end
    if action == 'reload' or action == 'unload' then
        for i, v in pairs(addons) do
            log(action .. ' ' .. v)
            windower.send_command('input //send @all //lua ' .. action .. ' ' .. v)
        end
        return
    end
    if action == 'sh' or action == 'sh1' then
        windower.send_command('input //sparks buyall acheron shield')
    elseif action == 'sh2' then
        windower.send_command('input //sparks buyall ice shield')
    elseif action == 'powd' or action == 'acc' then
        if secondary ~= nil then
            windower.send_command('input //buypowder ' .. secondary)
        else
            windower.send_command('input //buypowder 99999')
        end
    end
    for i, v in pairs(sell_items) do
        windower.send_command('input //sellnpc ' .. v)
    end
end

function spell(spell)
    windower.send_command('input //send @all //ma ' .. spell .. ' me')
end

-- warpall
function warpall()
    windower.send_command('input //send @all //warp')
end

local commands = T{
    ["ab"] = autobattle,
    ["add"] = addons,
    ["addons"] = addons,
    ["ambu"] = ambu,
    ["at"] = attack,
    ["atk"] = attack,
    ["att"] = attack,
    ["attack"] = attack,
    ["buffs"] = buffs,
    ["coal"] = coalition_warp,
    ["co"] = coalition_warp,
    ["cp"] = cp_cape,
    ["debug"] = debug,
    ["di"] = domain_invasion,
    ["fast"] = fast,
    ["follow"] = follow,
    ["fo"] = follow,
    ["gear"] = gear_up,
    ["inv"] = invite,
    ["invis"] = invisible,
    ["logout"] = logout,
    ["log"] = logout,
    ["lul"] = lullaby,
    ["lullaby"] = lullaby,
    ["mer"] = merits,
    ["merit"] = merits,
    ["merits"] = merits,
    ["mo"] = mount, 
    ["mou"] = mount,
    ["mount"] = mount,
    ["porter"] = porter,
    ["repeater"] = repeater,
    ["rpt"] = repeater,
    ["rings"] = rings,
    ["shutdown"] = shutdown,
    ["sneak"] = sneak,
    ["sp"] = spell,
    ["spk"] = sparks,
    ["sprk"] = sparks,
    ["spark"] = sparks,
    ["sparks"] = sparks,
    ["warp"] = warpall,
    ["warpall"] = warpall,
}

windower.register_event('addon command', function(command1, command2, command3, command4, command5, ...)
    command1 = command1 and command1:lower()
    command2 = command2 and command2:lower()
    command3 = command3 and command3:lower()
    command4 = command4 and command4:lower()
    command5 = command5 and command5:lower()

    cmd_remainder = table.concat({...}, ' ')

    target_operation = commands[command1]
    if command1 == nil then
        log("that's a base command")
    elseif target_operation == nil then
        log('command is bad: "' .. command1 .. '"')
    else
        target_operation(command2, command3, command4, command5)
    end
end)