_addon.name = 'Jumper'
_addon.author = 'modle'
_addon.version = '0.1.0'
_addon.commands = {'jumps', 'ju'}

require('logger')

-- every jump, send status about jumps
-- when jump ready, send notice
-- jump commands:
    -- //jumps normal
    -- //jumps threat
-- when jump command, try to use in order in list

-- need this format if we want to use numbers as indices
local threat_jumps = {[159] = "High Jump", [167] = "Soul Jump"}
local normal_jumps = {[158] = "Jump", [166] = "Spirit Jump"}
-- this format is required for jump_types:containskey(somestring)
local jump_types = T{
    threat = threat_jumps,
    normal = normal_jumps,
}
local target_padding = 20

function dojumps(type)
    local recasts = windower.ffxi.get_ability_recasts()

    target_ability = ""
    for k,v in pairs(jump_types[type]) do
        if (recasts[k] == 0 and target_ability == "") then
            target_ability = v
        end
    end

    if target_ability ~= "" then
        windower.send_command('input /ja "' .. target_ability .. '" <t>')
    end

    coroutine.sleep(2)
    check_ready()
end

function check_ready()
    local recasts = windower.ffxi.get_ability_recasts()
    local current_recasts = {}
    for k,v in pairs(jump_types) do
        for key,val in pairs(v) do
            local to_pad = target_padding - string.len(val)
            local notice_string = ""
            if (recasts[key]) == 0 then
                notice_string = "READY"
            else
                notice_string = recasts[key] or "N/A"
            end
            padded_so_far = 0
            while padded_so_far < to_pad do
                notice_string = " " .. notice_string
                padded_so_far = padded_so_far + 1
            end
            current_recasts[val] = notice_string
        end
    end
    for k,v in pairs(current_recasts) do
        log(k .. ":" .. v)
    end
end

windower.register_event('addon command', function(command1, ...)
    command1 = command1 and command1:lower() or 'help'
    if command1 == 'help' then
        print('%s v%s':format(_addon.name, _addon.version))
        print('    \\cs(255,255,255)no help for you\\cr - yo')
    elseif jump_types:containskey(command1) then
        dojumps(command1)
    end
end)
