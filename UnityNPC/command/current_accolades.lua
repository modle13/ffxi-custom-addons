current_accolades = 99999

function set_accolades(new_amount)
    current_accolades = new_amount
end

function get_current_accolades()
    return current_accolades
end

windower.register_event('addon command', function(command1, command2, ...)
    local command1 = command1 and command1:lower() or 'help'
    if command1 == 'acc' then
        current_accolades = tonumber(command2)
        log('set current accolades to ' .. current_accolades)
    end
end)