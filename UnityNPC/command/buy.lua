local NilCommand = require('command/nil')
local EntityFactory = require('model/entity/factory')
local DialogueFactory = require('model/dialogue/factory')
require('logger')
require('command/current_accolades')
res = require('resources')


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
local BuyCommand = NilCommand:NilCommand()
BuyCommand.__index = BuyCommand


--------------------------------------------------------------------------------
function BuyCommand:BuyCommand(id, item, zone, count)
    local o = NilCommand:NilCommand()
    setmetatable(o, self)

    -- get current bag space
    bag_ids = res.bags:key_map(string.gsub-{' ', ''} .. string.lower .. table.get-{'english'} .. table.get+{res.bags}):map(table.get-{'id'})
    inventory = windower.ffxi.get_items(bag_ids.inventory)

    o._id = id
    o._item = item
    o._zone = zone

    if item.en == 'Prize Powder' then
        o._count = (inventory.max - inventory.count) * 99
        if count < o._count then
            log('asked for fewer than have space for; adjusting down')
            o._count = count
        else
            local cost = o._count * 10
            if cost > get_current_accolades() then
                o._count = math.floor(current_accolades / 10)
                log('cost too high, adjusting')
            end
            new_accolade_count = current_accolades - o._count * 10
            set_accolades(new_accolade_count)
            log('remaining accolades: ' .. current_accolades)
        end
    else
        o._count = count
    end

    log('setting count to ' .. o._count .. ' for ' .. item.en)
    o._type = 'BuyCommand'

    o._dialogue = DialogueFactory.CreateBuyDialogue(EntityFactory.CreateMob(o._id, o._zone),
        EntityFactory.CreatePlayer(), o._item, o._count)
    o._dialogue:SetSuccessCallback(function() o._on_success() end)
    o._dialogue:SetFailureCallback(function() o._on_failure() end)

    return o
end

--------------------------------------------------------------------------------
function BuyCommand:OnIncomingData(id, pkt)
    return self._dialogue:OnIncomingData(id, pkt)
end

--------------------------------------------------------------------------------
function BuyCommand:OnOutgoingData(id, pkt)
    return self._dialogue:OnOutgoingData(id, pkt)
end

--------------------------------------------------------------------------------
function BuyCommand:IsSimple()
    return false
end

--------------------------------------------------------------------------------
function BuyCommand:__call(state)
    self._dialogue:Start()
end

return BuyCommand